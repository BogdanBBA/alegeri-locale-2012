﻿unit EditorFP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TFEdFP = class(TForm)
    exitB: TButton;
    Button1: TButton;
    locL: TLabel;
    lb: TListBox;
    le1: TLabeledEdit;
    le2: TLabeledEdit;
    le3: TLabeledEdit;
    Label1: TLabel;
    IDErrSH: TShape;
    colSH: TShape;
    cld: TColorDialog;
    Button2: TButton;
    saveT: TTimer;
    Button3: TButton;
    Button4: TButton;
    Label13: TLabel;
    procedure exitBClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lbClick(Sender: TObject);
    procedure le1Change(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure saveTTimer(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FEdFP: TFEdFP;

implementation

{$R *.dfm}

uses AL12_pas, DataTypes, functii;

procedure TFEdFP.Button1Click(Sender: TObject);
var x: integer;
begin
  x:=FPozPartidInFP(lb.Items[lb.ItemIndex]); if x=-1 then begin showmessage('ERROR: Can not find party "'+lb.Items[lb.ItemIndex]+'" in list (by FPozPartidInFP)'); exit end;
  fp[x].NumeScurt:=le1.Text; fp[x].NumeComplet:=le2.Text; fp[x].NumeDinListeOficial:=le3.Text;
  fp[x].Culoare:=colSH.Brush.Color; SaveDataToXML('data'); lb.Items[lb.ItemIndex]:=fp[x].NumeScurt;
  lb.Enabled:=false; Button1.Caption:='Salvat!'; Button1.Enabled:=false; saveT.Enabled:=true
end;

procedure TFEdFP.Button2Click(Sender: TObject);
begin
  cld.Color:=colSH.Brush.Color; if cld.Execute then colSH.Brush.Color:=cld.Color
end;

procedure TFEdFP.Button3Click(Sender: TObject);
var i: word; x: integer;
begin
  if nfp<2 then begin showmessage('Nu poți șterge singurul partid din baza de date.'); exit end;
  x:=FPozPartidinFP(lb.Items[lb.ItemIndex]); if x=-1 then begin showmessage('ERROR: Can not find party "'+lb.Items[lb.ItemIndex]+'" in list (by FPozPartidInFP)'); exit end;
  if MessageDlg('Ești sigur că vrei să ștergi '+fp[x].NumeScurt+' ("'+fp[x].NumeComplet+'")?', mtConfirmation, mbYesNo, 0)=mrYes then
    begin
      for i:=x to nfp-2 do fp[i]:=fp[i+1]; inc(nfp, -1); setlength(fp, nfp);
      SaveDatatoXML('data'); showmessage('Succes!'); FEdFP.OnShow(Button3)
    end
end;

procedure TFEdFP.Button4Click(Sender: TObject);
begin F1.Adugareformaiunepolitic1Click(Button4) end;

procedure TFEdFP.exitBClick(Sender: TObject);
begin
  FEdFP.Close; F1.refBClick(FEdFP)
end;

procedure TFEdFP.FormShow(Sender: TObject);
var i: word;
begin
  Label13.Caption:='Formațiuni: '+inttostr(nfp); lb.Clear; lb.Sorted:=true; for i:=0 to nfp-1 do lb.Items.Add(fp[i].NumeScurt); lb.ItemIndex:=0; lbClick(FEdFP)
end;

procedure TFEdFP.lbClick(Sender: TObject);
var x: integer;
begin
  x:=FPozPartidInFP(lb.Items[lb.ItemIndex]); if x=-1 then exit;
  le1.EditLabel.Caption:='Nume scurt (unic, folosit ca ID) (I='+inttostr(x)+')';
  le1.Text:=fp[x].NumeScurt; le2.Text:=fp[x].NumeComplet; le3.Text:=fp[x].NumeDinListeOficial;
  colSH.Brush.Color:=fp[x].Culoare
end;

procedure TFEdFP.le1Change(Sender: TObject);
var x: word; r: integer;
begin
  r:=-1; for x:=0 to nfp-1 do if (fp[x].NumeScurt=le1.Text) and (x<>FPozPartidInFP(lb.Items[lb.ItemIndex])) then begin r:=x; break end;
  if r<>-1 then showmessage('Nu poți folosi numele ăsta pentru că aparține deja formațiunii politice cu I='+inttostr(r)+', "'+fp[r].NumeComplet+'"');
  IDErrSH.Visible:=(r<>-1) or (le1.Text=''); Button1.Enabled:=not IDErrSH.Visible
end;

procedure TFEdFP.saveTTimer(Sender: TObject);
begin
  saveT.Enabled:=false; lb.Enabled:=true; Button1.Enabled:=true; Button1.Caption:='Salvare'
end;

end.
