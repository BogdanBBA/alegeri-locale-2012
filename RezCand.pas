﻿unit RezCand;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, TeEngine, TeeProcs, Chart, ExtCtrls, Series, Menus, ExtDlgs;

type
  TFRCand = class(TForm)
    ccb: TComboBox;
    closeB: TButton;
    Panel2: TPanel;
    alesL: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    rb1: TRadioButton;
    rb2: TRadioButton;
    ch: TChart;
    Series1: TBarSeries;
    Label3: TLabel;
    Label4: TLabel;
    Cimg: TImage;
    opi: TOpenPictureDialog;
    procedure FormShow(Sender: TObject);
    procedure closeBClick(Sender: TObject);
    procedure ccbChange(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure chClickSeries(Sender: TCustomChart; Series: TChartSeries;
      ValueIndex: Integer; Button: TMouseButton; Shift: TShiftState; X,
      Y: Integer);
    procedure Label8Click(Sender: TObject);
    procedure Label3Click(Sender: TObject);
    procedure CimgClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRCand: TFRCand;

implementation

{$R *.dfm}

uses AL12_pas, DataTypes, functii, RezLoc, RezFP;

procedure TFRCand.closeBClick(Sender: TObject);
begin
  FRCand.Close;
  F1.MouseOverCity(F1.FindComponent(lastCC))
end;

procedure TFRCand.FormResize(Sender: TObject);
begin
  closeB.Left:=FRCand.Width-closeB.Width-31; Panel2.Width:=closeB.Left-Panel2.Left+closeB.Width;
  ch.Width:=closeB.Left-ch.Left+closeB.Width; ch.Height:=FRCand.Height-ch.Top-40
end;

procedure TFRCand.FormShow(Sender: TObject);
var i, j: word;
begin
  ccb.Items.Clear; ccb.Sorted:=true;
  for i:=0 to nvot-1 do for j:=0 to vot[i].Candidati.nc-1 do
    if ccb.Items.IndexOf(vot[i].Candidati.Candidat[j].Nume)=-1 then ccb.Items.Add(vot[i].Candidati.Candidat[j].Nume);
  ccb.ItemIndex:=ccb.Items.IndexOf(candShow); ccbChange(FRCand)
end;

procedure TFRCand.Label3Click(Sender: TObject);
begin fpShow:=TLabel(Sender).Caption; if FRFP.Showing then FRFP.OnShow(Label3) else FRFP.Show; FRFP.SetFocus end;

procedure TFRCand.Label8Click(Sender: TObject);
begin locShow:=TLabel(Sender).Caption; if FRL.Showing then FRL.OnShow(Label8) else FRL.Show; FRL.SetFocus end;

procedure LaL(x, y: TLabel);
begin y.Left:=x.Left+x.Width end;

procedure TFRCand.chClickSeries(Sender: TCustomChart; Series: TChartSeries;
  ValueIndex: Integer; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var an, a, b: integer; sx: string;
begin
  an:=strtoint(copy(Series.Labels[ValueIndex], pos('(', Series.Labels[ValueIndex])+1, pos(')', Series.Labels[ValueIndex])-pos('(', Series.Labels[ValueIndex])-1));
  FCandidat(ccb.Text, an, a, b); alesL.Caption:=ccb.Text;
  Label3.Caption:=vot[a].Candidati.Candidat[b].Politica; Label8.Caption:=vot[a].Unde;
  Label4.Caption:=' in '+inttostr(vot[a].Year)+' pentru '+Functie(vot[a].Functie)+' la ';
  Lal(Label6, Label3); Lal(Label3, Label4); Lal(Label4, Label8);
  if (vot[a].TotalVoturi<>0) and (vot[a].Candidati.Candidat[b].NrVoturi<>0) then sx:=' ('+floattostr(vot[a].Candidati.Candidat[b].Procentaj)+'%)' else sx:='?';
  Label7.Caption:='A obținut '+grupat(vot[a].Candidati.Candidat[b].NrVoturi)+sx+' din '+grupat(vot[a].TotalVoturi)+' voturi'
end;

procedure TFRCand.CimgClick(Sender: TObject);
var sx: string; ok: boolean;
begin
  if fileexists('img\P_'+ccb.Text+'.jpg') then
    if MessageDlg('Imaginea pentru acest candidat există deja...'+dnl+'Doriți s-o înlocuiți?', mtConfirmation, mbYesNo, 0)<>mrYes then exit;
  opi.InitialDir:=GetDesktopFolder;
  if opi.Execute then
    begin
      sx:=opi.Filename;
      if pos('HTTP://', ansiuppercase(sx))<>0 then ok:=download(sx, 'img\P_'+ccb.Text+'.jpg')
      else ok:=copyfile(pchar(sx), pchar('img\P_'+ccb.Text+'.jpg'), false);
      if ok then ccbChange(Cimg) else showmessage('Imaginea nu a fost copiată cu succes. Încearcă s-o copiezi manual.')
    end
end;

procedure TFRCand.ccbChange(Sender: TObject);
var i, j, x, y, ncand, nctg: integer; nv, tv: longint; r: extended; rx, sx, tx, vx: string; l: TStringList; b: TBarSeries;
begin
  FCandidat(ccb.Text, x, y); if (x=-1) or (y=-1) then begin showmessage('Nu am informații despre candidatul pe care l-ai selectat !'); exit end;
  try Cimg.Picture.LoadFromFile('img\P_'+ccb.Text+'.jpg') except Cimg.Picture.LoadFromFile('img\P'+vot[x].Candidati.Candidat[y].Sex+'.jpg') end;
  //
  ch.ClearChart; ch.View3D:=false;
  ch.Title.Font.Name:='Ubuntu'; ch.Title.Font.Style:=[fsBold]; ch.Title.Font.Size:=13; ch.Title.Font.Color:=$00522914;
  ch.SubTitle.Font:=ch.Title.Font; ch.SubTitle.Font.Size:=10; ch.SubTitle.Font.Color:=$007D532D;
  b:=TBarSeries.Create(Self); b.Active:=true;
  ncand:=0; nv:=0; tv:=0; l:=TStringList.Create; l.Duplicates:=dupIgnore; l.Sorted:=false; sx:=''; tx:=''; vx:='';
  for i:=0 to nvot-1 do for j:=0 to vot[i].Candidati.nc-1 do if vot[i].Candidati.Candidat[j].Nume=vot[x].Candidati.Candidat[y].Nume then
    begin
      inc(ncand); if l.IndexOf(vot[i].Functie)=-1 then l.Add(vot[i].Functie); inc(nv, vot[i].Candidati.Candidat[j].NrVoturi); inc(tv, vot[i].TotalVoturi); if j=0 then inc(nctg);
      if rb1.Checked then begin r:=vot[i].Candidati.Candidat[j].NrVoturi; rx:=grupat(vot[i].Candidati.Candidat[j].NrVoturi) end
      else begin r:=vot[i].Candidati.Candidat[j].Procentaj; rx:=floattostr(r)+'%' end;
      b.AddBar(r, Functie(vot[i].Functie)+' ('+inttostr(vot[i].Year)+') ['+rx+']', fp[FPozPartidInFP(vot[i].Candidati.Candidat[j].Politica)].Culoare)
    end;
  ch.AddSeries(b);
  ch.Title.Caption:='Rezultatele candidaturilor lui '+vot[x].Candidati.Candidat[y].Nume;
  if ncand=1 then rx:='o candidatură' else rx:=inttostr(ncand)+' candidaturi'; if tv>0 then vx:=formatfloat(' (0.000%)', nv/tv*100);
  if nctg=0 then tx:='niciuna câștigată' else if nctg=1 then tx:='una câștigată' else tx:=inttostr(nctg)+' câștigate';
  if l.Count>1 then for i:=0 to l.Count-2 do sx:=sx+functie(l[i])+', '; if l.Count>0 then sx:=' (pentru '+sx+functie(l[l.Count-1])+')';
  ch.SubTitle.Caption:='Are '+rx+sx+', din care '+tx+'; a primit '+grupat(nv)+' din '+grupat(tv)+' voturi'+vx;
  Panel2.Visible:=b.Count>0; if Panel2.Visible then chClickSeries(ch, ch.Series[0], 0, mbLeft, [], 0, 0)
end;

end.
