object FEdCJ: TFEdCJ
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Editor Coordonate Jude'#539'e'
  ClientHeight = 426
  ClientWidth = 442
  Color = clBlack
  Font.Charset = ANSI_CHARSET
  Font.Color = clWhite
  Font.Height = -17
  Font.Name = 'Ubuntu'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  ScreenSnap = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 21
  object Label1: TLabel
    Left = 16
    Top = 8
    Width = 402
    Height = 21
    Caption = 'Alege un jude'#539', apoi d'#259' click pe hart'#259' pentru a-l fixa.'
  end
  object Label2: TLabel
    Left = 223
    Top = 48
    Width = 42
    Height = 22
    Caption = 'Jude'#539
    Font.Charset = ANSI_CHARSET
    Font.Color = clSilver
    Font.Height = -17
    Font.Name = 'Ubuntu'
    Font.Style = [fsItalic]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 223
    Top = 67
    Width = 52
    Height = 21
    Caption = 'Label3'
  end
  object Label4: TLabel
    Left = 223
    Top = 94
    Width = 86
    Height = 22
    Caption = 'Coordonate'
    Font.Charset = ANSI_CHARSET
    Font.Color = clSilver
    Font.Height = -17
    Font.Name = 'Ubuntu'
    Font.Style = [fsItalic]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 223
    Top = 113
    Width = 52
    Height = 21
    Caption = 'Label5'
  end
  object Label13: TLabel
    Left = 16
    Top = 402
    Width = 60
    Height = 22
    Caption = 'Label13'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -17
    Font.Name = 'Ubuntu'
    Font.Style = [fsItalic]
    ParentFont = False
  end
  object lb: TListBox
    Left = 16
    Top = 48
    Width = 201
    Height = 353
    Font.Charset = ANSI_CHARSET
    Font.Color = clGray
    Font.Height = -19
    Font.Name = 'Ubuntu'
    Font.Style = []
    ItemHeight = 24
    ParentFont = False
    TabOrder = 0
    OnClick = lbClick
  end
  object closeB: TButton
    Left = 223
    Top = 360
    Width = 195
    Height = 41
    Caption = #206'nchide'
    Font.Charset = ANSI_CHARSET
    Font.Color = clSilver
    Font.Height = -23
    Font.Name = 'Ubuntu'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = closeBClick
  end
  object saveB: TButton
    Left = 223
    Top = 313
    Width = 195
    Height = 41
    Caption = 'Salvare coord.'
    Font.Charset = ANSI_CHARSET
    Font.Color = clSilver
    Font.Height = -23
    Font.Name = 'Ubuntu'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    OnClick = saveBClick
  end
  object saveT: TTimer
    Enabled = False
    Interval = 2000
    OnTimer = saveTTimer
    Left = 376
    Top = 264
  end
end
