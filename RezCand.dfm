object FRCand: TFRCand
  Left = 0
  Top = 0
  BorderIcons = [biMinimize, biMaximize]
  Caption = 'Rezultate candida'#539'i'
  ClientHeight = 414
  ClientWidth = 826
  Color = clBlack
  Font.Charset = ANSI_CHARSET
  Font.Color = clWhite
  Font.Height = -16
  Font.Name = 'Ubuntu'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  ScreenSnap = True
  WindowState = wsMaximized
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 20
  object Label1: TLabel
    Left = 144
    Top = 59
    Width = 65
    Height = 20
    Caption = 'Nr. voturi'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -16
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 144
    Top = 83
    Width = 58
    Height = 20
    Caption = 'Procent'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -16
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
  end
  object Cimg: TImage
    Left = 8
    Top = 8
    Width = 114
    Height = 141
    Cursor = crHandPoint
    Center = True
    Proportional = True
    Stretch = True
    OnClick = CimgClick
  end
  object ccb: TComboBox
    Left = 128
    Top = 22
    Width = 506
    Height = 33
    Style = csDropDownList
    DropDownCount = 20
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -21
    Font.Name = 'Ubuntu Light'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnChange = ccbChange
  end
  object closeB: TButton
    Left = 607
    Top = 8
    Width = 150
    Height = 57
    Caption = #206'nchide'
    Font.Charset = ANSI_CHARSET
    Font.Color = 14337153
    Font.Height = -27
    Font.Name = 'Ubuntu'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = closeBClick
  end
  object Panel2: TPanel
    Left = 288
    Top = 61
    Width = 398
    Height = 88
    BevelOuter = bvNone
    TabOrder = 2
    object alesL: TLabel
      Left = 18
      Top = 1
      Width = 61
      Height = 31
      Caption = 'alesL'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -24
      Font.Name = 'Ubuntu'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 18
      Top = 32
      Width = 74
      Height = 21
      Caption = 'Candidat '
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -17
      Font.Name = 'Ubuntu'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 18
      Top = 56
      Width = 48
      Height = 18
      Caption = 'Label7'
      Font.Charset = ANSI_CHARSET
      Font.Color = 16766429
      Font.Height = -16
      Font.Name = 'Ubuntu Mono'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel
      Left = 236
      Top = 32
      Width = 52
      Height = 21
      Cursor = crHandPoint
      Caption = 'Label8'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8314055
      Font.Height = -17
      Font.Name = 'Ubuntu'
      Font.Style = []
      ParentFont = False
      OnClick = Label8Click
    end
    object Label3: TLabel
      Left = 85
      Top = 32
      Width = 53
      Height = 23
      Cursor = crHandPoint
      Caption = 'Label3'
      Font.Charset = ANSI_CHARSET
      Font.Color = clSilver
      Font.Height = -17
      Font.Name = 'Ubuntu Medium'
      Font.Style = []
      ParentFont = False
      OnClick = Label3Click
    end
    object Label4: TLabel
      Left = 154
      Top = 32
      Width = 52
      Height = 21
      Caption = 'Label4'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -17
      Font.Name = 'Ubuntu'
      Font.Style = []
      ParentFont = False
    end
  end
  object rb1: TRadioButton
    Left = 128
    Top = 61
    Width = 138
    Height = 17
    Checked = True
    TabOrder = 3
    TabStop = True
    OnClick = ccbChange
  end
  object rb2: TRadioButton
    Left = 128
    Top = 85
    Width = 138
    Height = 17
    TabOrder = 4
    OnClick = ccbChange
  end
  object ch: TChart
    Left = 8
    Top = 155
    Width = 601
    Height = 250
    Title.Text.Strings = (
      'TChart')
    OnClickSeries = chClickSeries
    View3D = False
    TabOrder = 5
    object Series1: TBarSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = True
      Gradient.Direction = gdTopBottom
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Bar'
      YValues.Order = loNone
    end
  end
  object opi: TOpenPictureDialog
    DefaultExt = '*.jpg;*.jpeg'
    Filter = '*.jpg;*.jpeg'
    Options = [ofHideReadOnly, ofExtensionDifferent, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Title = 
      'Selecta'#539'i imaginea (de pe hard-disk / de pe internet, printr-un ' +
      'link)'
    Left = 80
    Top = 96
  end
end
