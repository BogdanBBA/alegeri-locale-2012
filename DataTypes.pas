unit DataTypes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ShellAPI, Menus, ExtCtrls, jpeg, PngImage, ImgList,
  ComCtrls, MPlayer, ToolWin, XML.VerySimple;

const nl=#13#10; dnl=nl+nl;
  NFo: array[0..2] of string=('data', 'data\backups', 'img');
  NFi: array[0..2] of string=('data\data.xml', 'data\coord.xml', 'data\alegeri.xml');
  CCSize: array[1..3] of byte=(21, 14, 10);
  ncg=5;

type TLabelGroup=record
  nameL: TLabel;
  img: TImage;
end;

type TCandidatGroup=record
  p: TPanel;
  nameL, polL, procL, nrVotL: TLabel;
  candImg, polCandColImg: TImage;
end;
//
type TFormatiunePolitica=record
  NumeScurt, NumeComplet, NumeDinListeOficial: string;
  Culoare: TColor;
end;

type TCandidat=record
  Nume, Politica: string;
  NrVoturi: longint;
  Procentaj: real;
  Sex: char;
  Nascut: TDate;
  NascutUnde, Educatie, Profesie: string;
end;

type TListaCandidati=record
  nc: word;
  Candidat: array of TCandidat;
end;

type TLocatie=record
  Nume, JudetIfOras: string;
  CSizeIfOras, X, Y: word;
end;

type TVotare=record
  Year: word;
  Unde, Functie: string; //"Unde" trebuie sa se potiveasca cu tipul functiei (pCJ, P)
  Candidati: TListaCandidati;
  TotalVoturi: longint;
end;

var
  fp: array of TFormatiunePolitica;
  jc, oc: array of TLocatie;
  vot: array of TVotare;
  a: array of word;

  lg: array of TLabelGroup;
  cg: array of TCandidatGroup;
  cc: array of TShape;

  na, nfp, njc, noc, nvot: word;
  lastCC, fpShow, locShow, candShow: string;

  x, x2, x3: TVerySimpleXML;
  jl, ol: TStringList;
  f: textfile;

procedure log(s: string);
procedure ScanForFiles(directory, extension: string; var list: TStringList);
procedure ReadData;
procedure XMLtoData(typ: string; x: TVerySimpleXML);
procedure GenerateTextReport(OpenWhenDone: boolean);
procedure CheckData(NotifyProblems: boolean);
procedure SaveDataToXML(typ: string);
function FPozPartidInFP(s: string): integer;

implementation

uses AL12_pas, functii;

procedure log(s: string);
var logf: textfile;
begin
  try
    F1.logL.Caption:=s+formatdatetime(' (d mmm yy hh:nn)', now);
    assignfile(logf, 'data\log.txt'); append(logf); writeln(logf, formatdatetime('dd.mmm.yyyy hh:nn:ss    |    ', now), s); closefile(logf)
  except
    begin
      try
        F1.logL.Caption:=s+formatdatetime('"(Log retry, "d mmm yy hh:nn)', now);
        assignfile(logf, 'data\log.txt'); append(logf); writeln(logf, formatdatetime('dd.mmm.yyyy hh:nn:ss    |    "Log retry: "', now), s); closefile(logf)
      except on E:Exception do showmessage('Failed a second time to log message "'+s+'"'+dnl+e.classname+' :  '+e.Message) end
    end
  end
end;

procedure ScanForFiles(directory, extension: string; var list: TStringList);
var search: TSearchRec;
begin
  if list=nil then begin showmessage('List is nil! Exiting ScanForFiles()...'); exit end;
  log('Scanning for files...'); // Subdirectories
  if FindFirst(directory + '*.'+ansiuppercase(extension), faDirectory, search) = 0 then
  begin
    repeat
      if (search.Name[1] <> '.') then list.Add(search.Name)
    until FindNext(search) <> 0;
    FindClose(search)
  end;
  log('Scan in "'+directory+'" successfull. NFiles = '+inttostr(list.Count))
end;

procedure ReadData;
var i: word; phz: string;
begin
  for i:=0 to length(NFo)-1 do if not directoryexists(NFo[i]) then begin showmessage('The folder "'+NFo[i]+'" does not exist. That is a big error.'+dnl+'You probably have a faulty installation, so reinstall. The app will now close.'); Halt end;
  for i:=0 to length(NFi)-1 do if not fileexists(NFi[i]) then begin showmessage('The file "'+NFi[i]+'" does not exist. That is a big error.'+dnl+'You probably have a faulty installation, so reinstall. The app will now close.'); Halt end;
  DecimalSeparator:='.'; ThousandSeparator:=','; DateSeparator:='/'; TimeSeparator:=':';
  assignfile(f, 'data\log.txt'); rewrite(f); closefile(f);
  log('Initializing and reading data...');
  //
  try
  x:=TVerySimpleXML.Create; x2:=TVerySimpleXML.Create; x3:=TVerySimpleXML.Create;
  phz:='coord';   x.LoadFromFile('data\coord.xml'); XMLtoData('coord', x); //x.Free;
  phz:='data';    x2.LoadFromFile('data\data.xml');  XMLtoData('data', x2); //x2.Free;
  phz:='alegeri'; x3.LoadFromFile('data\alegeri.xml'); XMLtoData('alegeri', x3); //x3.Free;
  log('Data read successfully!')
  except on E:Exception do showmessage('ReadData() ERROR while converting to "'+phz+'"'+dnl+E.ClassName+': '+E.Message) end;
  //
  CheckData(false)
end;

procedure XMLtoData(typ: string; x: TVerySimpleXML);
  {*}function repS(s, t1, t2: string): string;
  begin result := stringreplace(s, t1, t2, [rfReplaceAll]) end;
  {*}procedure CandidXMLtoCandid(cxml: TXMLNode; var c: TCandidat);
  begin
    try c.Nume:=cxml.Attribute['nume'] except c.Nume:='Necunoscut' end;
    try c.Politica:=cxml.Find('politica').Text except c.Politica:='UNK' end;
    try c.NrVoturi:=strtoint(cxml.Find('nr_voturi').Text) except c.NrVoturi:=0 end;
    try c.Procentaj:=strtofloat(cxml.Find('procentaj').Text) except c.Procentaj:=0 end;
    try c.Sex:=cxml.Attribute['sex'][1] except c.Sex:='M' end;
    try c.Nascut:=strtodate(cxml.Attribute['nascut']) except c.Nascut:=encodedate(1900, 1, 1) end;
    try c.NascutUnde:=cxml.Attribute['nascut_unde'] except c.NascutUnde:='Necunoscut' end;
    try c.Educatie:=cxml.Attribute['educatie'] except c.Educatie:='Necunoscut' end;
    try c.Profesie:=cxml.Attribute['profesie'] except c.Profesie:='Necunoscut' end;
  end;
var i, j, k, n: word; phz: string; ok: boolean; cx: TCandidat; xn, xn2: TXmlNode; xnl, xnl2: TXmlNodeList;
begin
  log('XMLtoData(typ="'+typ+'")');
  case strtocase(typ, ['coord', 'data', 'alegeri']) of
  1: //TYP = COORD
    begin try
      noc:=0; setlength(oc, noc); njc:=x.Root.ChildNodes.Count; setlength(jc, njc);
      for i:=0 to njc-1 do
        begin
          xn:=x.Root.ChildNodes[i]; jc[i].JudetIfOras:='';
          try jc[i].Nume:=xn.Attribute['nume'] except jc[i].Nume:='Necunoscut' end; phz:=jc[i].Nume;
          try jc[i].X:=strtoint(xn.Attribute['coordX']) except jc[i].X:=0 end;
          try jc[i].Y:=strtoint(xn.Attribute['coordY']) except jc[i].Y:=0 end;
          if xn.ChildNodes.Count>0 then for j:=0 to xn.ChildNodes.Count-1 do
            begin
              xn2:=xn.ChildNodes[j]; inc(noc); setlength(oc, noc); oc[noc-1].JudetIfOras:=phz;
              try oc[noc-1].Nume:=xn2.Attribute['nume']+', '+jc[i].Nume except oc[noc-1].Nume:='Necunoscut, '+jc[i].Nume end;
              try oc[noc-1].X:=strtoint(xn2.Attribute['coordX']) except oc[noc-1].X:=0 end;
              try oc[noc-1].Y:=strtoint(xn2.Attribute['coordY']) except oc[noc-1].Y:=0 end;
              try oc[noc-1].CSizeIfOras:=strtoint(xn2.Attribute['CSize']) except oc[noc-1].CSizeIfOras:=2 end
            end
        end;
      //
      log('XMLtoData (coord) success; NJud='+inttostr(njc)+', NOrase='+inttostr(noc))
    except on E:Exception do showmessage('XMLtoData ERROR while converting to "'+typ+'"'+dnl+'Phase="'+phz+'"'+dnl+E.ClassName+': '+E.Message) end end;
  2: //TYP = DATA
    begin try
      //Politici
      xnl:=x.Root.FindNodes('formatiune'); nfp:=xnl.Count; setlength(fp, nfp);
      if nfp>0 then for i:=0 to nfp-1 do
        begin
          try fp[i].NumeScurt:=xnl[i].Find('nume_scurt').Text except fp[i].NumeScurt:='UNK' end;
          try fp[i].NumeComplet:=xnl[i].Find('nume_complet').Text except fp[i].NumeComplet:='Necunoscut' end;
          try fp[i].NumeDinListeOficial:=xnl[i].Find('nume_oficial').Text except fp[i].NumeDinListeOficial:='NECUNOSCUT' end;
          try fp[i].Culoare:=HtmlToColor(xnl[i].Find('culoare').Text) except fp[i].Culoare:=clGray end;
        end;
      //Lista Alegeri
      xnl:=x.Root.FindNodes('alegeri'); na:=xnl.Count; setlength(a, na);
      if na>0 then for i:=0 to na-1 do a[i]:=strtoint(xnl[i].Attribute['an']);
      //
      log('XMLtoData (data) success; NFP='+inttostr(nfp)+', NA='+inttostr(na))
    except on E:Exception do showmessage('XMLtoData ERROR while converting to "'+typ+'"'+dnl+E.ClassName+': '+E.Message) end end;
  3: //ALEGERI
    begin try
      xnl:=x.Root.FindNodes('Votare'); nvot:=xnl.Count; setlength(vot, nvot);
      if nvot>0 then for i:=0 to nvot-1 do
        begin
          try vot[i].Year:=strtoint(xnl[i].Attribute['an']) except vot[i].Year:=1900 end;
          try vot[i].Unde:=xnl[i].Attribute['unde'] except vot[i].Unde:='Necunoscut' end;
          try vot[i].Functie:=xnl[i].Attribute['functie'] except vot[i].Functie:='Necunoscut' end;
          vot[i].Candidati.nc:=xnl[i].ChildNodes.Count; setlength(vot[i].Candidati.Candidat, vot[i].Candidati.nc);
          if vot[i].Candidati.nc>0 then for j:=0 to vot[i].Candidati.nc-1 do CandidXMLtoCandid(xnl[i].ChildNodes[j], vot[i].Candidati.Candidat[j]);
          //
          if (vot[i].Year=1900) or (vot[i].Unde='Necunoscut') or (vot[i].Functie='Necunoscut') then showmessage('Conversion problem for Alegeri i='+inttostr(i));
        end;
      //ordonare candidati dupa rezultat
      if nvot>0 then for k:=0 to nvot-1 do
        if vot[k].Candidati.nc>1 then for i:=0 to vot[k].Candidati.nc-2 do for j:=i+1 to vot[k].Candidati.nc-1 do
          if vot[k].Candidati.Candidat[i].Procentaj<vot[k].Candidati.Candidat[j].Procentaj then
            begin cx:=vot[k].Candidati.Candidat[i]; vot[k].Candidati.Candidat[i]:=vot[k].Candidati.Candidat[j]; vot[k].Candidati.Candidat[j]:=cx end;
      //calc voturi totale (dupa nr.voturi/procent castigator)
      if nvot>0 then for k:=0 to nvot-1 do
        if vot[k].Candidati.nc>0 then
          begin
            if vot[k].Candidati.Candidat[0].NrVoturi<>0 then vot[k].TotalVoturi:=round((100/vot[k].Candidati.Candidat[0].Procentaj)*vot[k].Candidati.Candidat[0].NrVoturi);
            if vot[k].Candidati.nc>1 then for j:=1 to vot[k].Candidati.nc-1 do
              if vot[k].Candidati.Candidat[j].NrVoturi=0 then vot[k].Candidati.Candidat[j].NrVoturi:=round(vot[k].Candidati.Candidat[j].Procentaj*vot[k].TotalVoturi/100)
          end;
      //
      jl:=TStringList.Create; ol:=TStringList.Create;
      for i:=0 to njc-1 do jl.Add(jc[i].Nume); for i:=0 to noc-1 do ol.Add(oc[i].Nume);
      //
      log('XMLtoData (alegeri) success; NVotari='+inttostr(nvot))
    except on E:Exception do showmessage('XMLtoData ERROR while converting to "'+typ+'"'+dnl+'Phase="'+phz+'"'+dnl+E.ClassName+': '+E.Message) end end;
  else showmessage('XMLtoData ERROR: unknown typ="'+typ+'"')
  end
end;

procedure GenerateTextReport(OpenWhenDone: boolean);
const sepl='----------------------------------------------------';
var i, j: word; tc, tv: longint;
begin
  log('Generating text report...'); assignfile(f, 'data\report.txt'); rewrite(f);
  writeln(f, sepl+nl+'TEXT REPORT'+dnl+'Generat '+formatdatetime('dddd, d mmmm yyyy, h:nn:ss', now)+nl+sepl+nl);
  writeln(f, sepl+nl+'JUDETE ('+inttostr(njc)+')'+nl+sepl); for i:=0 to njc-1 do writeln(f, inttostr(i)+'. '+jc[i].Nume+' (X='+inttostr(jc[i].X)+', Y='+inttostr(jc[i].Y)+')');
  writeln(f, sepl+nl+'ORASE ('+inttostr(noc)+')'+nl+sepl); for i:=0 to noc-1 do writeln(f, inttostr(i)+'. '+oc[i].Nume+' (X='+inttostr(oc[i].X)+', Y='+inttostr(oc[i].Y)+')');
  writeln(f, sepl+nl+'FORMATIUNI POLITICE ('+inttostr(nfp)+')'+nl+sepl); for i:=0 to nfp-1 do writeln(f, inttostr(i)+'. '+fp[i].NumeScurt+' ("'+fp[i].NumeComplet+'", "'+fp[i].NumeDinListeOficial+'", '+ColortoHTML(fp[i].Culoare)+')');
  writeln(f, sepl+nl+'ALEGERI ('+inttostr(na)+')'+nl+sepl); for i:=0 to na-1 do writeln(f, inttostr(i)+'. '+inttostr(a[i]));
  tc:=0; for i:=0 to nvot-1 do inc(tc, vot[i].Candidati.nc);
  tv:=0; for i:=0 to nvot-1 do for j:=0 to vot[i].Candidati.nc-1 do inc(tv, vot[i].Candidati.Candidat[j].NrVoturi);
  writeln(f, sepl+nl+'VOTARI ('+inttostr(nvot)+', cu un total de '+grupat(tc)+' candidati si '+grupat(tv)+' voturi)'+nl+sepl);
  for i:=0 to nvot-1 do
    begin
      writeln(f, inttostr(i)+'. In '+inttostr(vot[i].Year)+' la '+vot[i].Unde+' pentru '+vot[i].Functie+': '+grupat(vot[i].TotalVoturi)+' vot(uri) pentru '+inttostr(vot[i].Candidati.nc)+' candidat(i)');
      for j:=0 to vot[i].Candidati.nc-1 do writeln(f, '    - '+vot[i].Candidati.Candidat[j].Nume+' ('+vot[i].Candidati.Candidat[j].Politica+') = '+grupat(vot[i].Candidati.Candidat[j].NrVoturi)+' vot(uri) ['+floattostr(vot[i].Candidati.Candidat[j].Procentaj)+'%]')
    end;
  closefile(f); log('Report generation successful!'); i:=0;
  if OpenWhenDone then ShellExecute(i, 'open', PChar('data\report.txt'), nil, nil, SW_SHOW)
end;

procedure CheckData(NotifyProblems: boolean);
//var
begin
  //log('Checking data...');
end;

procedure SaveDataToXML(typ: string);
  {*}function known(s: string): boolean;
  begin result := (s<>'') and (s<>' ') and (s<>'  ') and (ansiuppercase(s)<>'UNK') and (ansilowercase(s)<>'unknown') end;
  {*}procedure CandToCandXML(c: TCandidat; k: word; var cxml: TXMLNodeList);
  begin
    try        showmessage('k='+inttostr(k)+', '+c.Nume+' ('+c.Politica+')');
    cxml[k].AddChild('nume'); cxml[k].Find('nume').Text:=c.Nume;
    cxml[k].AddChild('politica'); cxml[k].Find('politica').Text:=c.Politica;
    cxml[k].AddChild('procentaj'); cxml[k].Find('procentaj').Text:=floattostr(c.Procentaj)
    except on E:Exception do showmessage('CandToCandXML ERROR for k='+inttostr(k)+dnl+E.ClassName+': '+E.Message) end
  end;
var i, j, k: word; phz: string;
begin
  log('SaveDataToXML(typ="'+typ+'")'); x:=TVerySimpleXML.Create;
  case strtocase(typ, ['coord', 'data', 'alegeri']) of
  1: //Save COORD
    begin try  phz:='backup';
      if not copyfile(pwidechar('data\coord.xml'), pwidechar('data\backups\coord '+formatdatetime('yyyy-mm-dd hh.nn', now)+'.xml'), false) then
        if MessageDlg('File "coord.xml" file backup has failed. Continue?', mtWarning, mbYesNo, 0)<>mrYes then exit;
      x.Root.NodeName:='CoordLocatii'; x.Root.SetAttribute('modified', formatdatetime('dddd, d mmmm yyyy, hh:nn', now));
      //
      for i:=0 to njc-1 do
        begin
          x.Root.AddChild('judet');
          with x.Root.ChildNodes[i] do
            begin
              SetAttribute('nume', jc[i].Nume); SetAttribute('coordX', inttostr(jc[i].X)); SetAttribute('coordY', inttostr(jc[i].Y)); k:=0;
              for j:=0 to noc-1 do if oc[j].JudetIfOras=jc[i].Nume then
                begin AddChild('oras'); inc(k); with ChildNodes[k-1] do begin SetAttribute('nume', copy(oc[j].Nume, 1, pos(', ', oc[j].Nume)-1)); SetAttribute('coordX', inttostr(oc[j].X)); SetAttribute('coordY', inttostr(oc[j].Y)); SetAttribute('CSize', inttostr(oc[j].CSizeIfOras)) end end
            end
        end;
      //
      x.SaveToFile('data\coord.xml'); log('SaveDataToXML (coord) success')
    except on E:Exception do showmessage('SaveDataToXML ERROR while saving "'+typ+'"'+dnl+'Phase "'+phz+'"'+dnl+E.ClassName+': '+E.Message) end end;
  2: //Save DATA
    begin try
      if not copyfile(pwidechar('data\data.xml'), pwidechar('data\backups\data '+formatdatetime('yyyy-mm-dd hh.nn', now)+'.xml'), false) then
        if MessageDlg('File "data.xml" file backup has failed. Continue?', mtWarning, mbYesNo, 0)<>mrYes then exit;
      x.Root.NodeName:='AlegeriData'; x.Root.SetAttribute('modified', formatdatetime('dddd, d mmmm yyyy, hh:nn', now));
      //
      for i:=0 to nfp-1 do
        begin
          x.Root.AddChild('formatiune');
          with x.Root.ChildNodes[i] do
            begin
              SetAttribute('id', inttostr(i));
              AddChild('nume_scurt'); Find('nume_scurt').Text:=fp[i].NumeScurt;
              AddChild('nume_complet'); Find('nume_complet').Text:=fp[i].NumeComplet;
              AddChild('nume_oficial'); Find('nume_oficial').Text:=fp[i].NumeDinListeOficial;
              AddChild('culoare'); Find('culoare').Text:=ColortoHTML(fp[i].Culoare)
            end
        end;
      for i:=0 to na-1 do
        begin x.Root.AddChild('alegeri'); x.Root.ChildNodes[nfp+i].SetAttribute('an', inttostr(a[i])) end;
      //
      x.SaveToFile('data\data.xml'); log('SaveDataToXML (data) success')
    except on E:Exception do showmessage('SaveDataToXML ERROR while saving "'+typ+'"'+dnl+'Phase "'+phz+'"'+dnl+E.ClassName+': '+E.Message) end end;
  3: //Save ALEGERI
    begin try  phz:='backup';
      if not copyfile(pwidechar('data\alegeri.xml'), pwidechar('data\backups\alegeri '+formatdatetime('yyyy-mm-dd hh.nn', now)+'.xml'), false) then
        if MessageDlg('File "alegeri.xml" file backup has failed. Continue?', mtWarning, mbYesNo, 0)<>mrYes then exit;
      x.Root.NodeName:='AlegeriRomania'; x.Root.SetAttribute('modified', formatdatetime('dddd, d mmmm yyyy, hh:nn', now));
      //convert cases
      for i:=0 to nvot-1 do for j:=0 to vot[i].Candidati.nc-1 do
        with vot[i].Candidati.Candidat[j] do Nume:=RightCase(Nume);
      //save
      for i:=0 to nvot-1 do
        begin
          x.Root.AddChild('Votare');
          with x.Root.ChildNodes[i] do
            begin
              SetAttribute('id', 'a'+inttostr(i)); SetAttribute('an', inttostr(vot[i].Year)); SetAttribute('functie', vot[i].Functie); SetAttribute('unde', vot[i].Unde);
              for j:=0 to vot[i].Candidati.nc-1 do
                begin
                  AddChild('candidat');
                  with ChildNodes[j] do
                    begin
                      SetAttribute('id', 'a'+inttostr(i)+'c'+inttostr(j));
                      SetAttribute('nume', vot[i].Candidati.Candidat[j].Nume); SetAttribute('sex', vot[i].Candidati.Candidat[j].Sex);
                      SetAttribute('nascut', datetostr(vot[i].Candidati.Candidat[j].Nascut)); SetAttribute('nascut_unde', vot[i].Candidati.Candidat[j].NascutUnde);
                      SetAttribute('educatie', vot[i].Candidati.Candidat[j].Educatie); SetAttribute('profesie', vot[i].Candidati.Candidat[j].Profesie);
                      AddChild('politica'); Find('politica').Text:=vot[i].Candidati.Candidat[j].Politica;
                      AddChild('nr_voturi'); Find('nr_voturi').Text:=inttostr(vot[i].Candidati.Candidat[j].NrVoturi);
                      AddChild('procentaj'); Find('procentaj').Text:=floattostr(vot[i].Candidati.Candidat[j].Procentaj)
                    end;
                end;
            end;
        end;
      //
      x.SaveToFile('data\alegeri.xml'); log('SaveDataToXML (alegeri) success')
    except on E:Exception do showmessage('SaveDataToXML ERROR while saving "'+typ+'"'+dnl+'Phase "'+phz+'"'+dnl+E.ClassName+': '+E.Message) end end;
  else showmessage('SaveDataToXML ERROR: unknown typ="'+typ+'"')
  end
end;

function FPozPartidInFP(s: string): integer;
var i: word;
begin
  result:=-1;
    for i:=0 to nfp-1 do
      if (ansiuppercase(fp[i].NumeScurt)=ansiuppercase(s)) or (ansiuppercase(fp[i].NumeComplet)=ansiuppercase(s)) or (ansiuppercase(fp[i].NumeDinListeOficial)=ansiuppercase(s)) then
        begin result:=i; break end
end;

end.

