object FAddRA: TFAddRA
  Left = 0
  Top = 0
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsSingle
  Caption = 'Ad'#259'ugare rezultate noi pentru alegerile locale'
  ClientHeight = 423
  ClientWidth = 881
  Color = clBlack
  Font.Charset = ANSI_CHARSET
  Font.Color = clWhite
  Font.Height = -16
  Font.Name = 'Ubuntu'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  ScreenSnap = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 20
  object locL: TLabel
    Left = 8
    Top = 8
    Width = 503
    Height = 34
    Caption = 'Completeaz'#259' datele '#537'i apas'#259' '#8221'Adaug'#259#8221
    Font.Charset = ANSI_CHARSET
    Font.Color = 14337153
    Font.Height = -29
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel
    Left = 40
    Top = 46
    Width = 46
    Height = 20
    Caption = 'Primar'
  end
  object Label2: TLabel
    Left = 40
    Top = 70
    Width = 100
    Height = 20
    Caption = 'Pre'#537'edinte CJ'
  end
  object Label3: TLabel
    Left = 40
    Top = 94
    Width = 102
    Height = 20
    Caption = 'Consiliul Local'
    Font.Charset = ANSI_CHARSET
    Font.Color = clSilver
    Font.Height = -16
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 40
    Top = 118
    Width = 131
    Height = 20
    Caption = 'Consiliul Jude'#539'ean'
    Font.Charset = ANSI_CHARSET
    Font.Color = clSilver
    Font.Height = -16
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 488
    Top = 62
    Width = 51
    Height = 20
    Caption = 'Alegeri'
  end
  object Label6: TLabel
    Left = 192
    Top = 118
    Width = 187
    Height = 20
    Caption = 'Exist'#259' deja ora'#537'ul selectat.'
    Font.Charset = ANSI_CHARSET
    Font.Color = 65408
    Font.Height = -16
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object Label7: TLabel
    Left = 192
    Top = 118
    Width = 623
    Height = 20
    Caption = 
      'Localitatea nu exist'#259' (nu are coordonate), dar va fi creat'#259' '#537'i o' +
      ' ve'#539'i putea edita mai t'#226'rziu.'
    Font.Charset = ANSI_CHARSET
    Font.Color = 33023
    Font.Height = -16
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object Label8: TLabel
    Left = 192
    Top = 144
    Width = 259
    Height = 20
    Caption = 'Textul din listele oficiale (2012, .XLS)'
  end
  object rb1: TRadioButton
    Left = 24
    Top = 48
    Width = 150
    Height = 17
    Checked = True
    TabOrder = 0
    TabStop = True
    OnClick = rb1Click
  end
  object rb2: TRadioButton
    Left = 24
    Top = 72
    Width = 150
    Height = 17
    Enabled = False
    TabOrder = 1
  end
  object rb3: TRadioButton
    Left = 24
    Top = 96
    Width = 150
    Height = 17
    Enabled = False
    TabOrder = 2
  end
  object rb4: TRadioButton
    Left = 24
    Top = 120
    Width = 150
    Height = 17
    Enabled = False
    TabOrder = 3
  end
  object closeB: TButton
    Left = 24
    Top = 344
    Width = 150
    Height = 57
    Caption = #206'nchide'
    Font.Charset = ANSI_CHARSET
    Font.Color = 14337153
    Font.Height = -27
    Font.Name = 'Ubuntu'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    OnClick = closeBClick
  end
  object addB: TButton
    Left = 24
    Top = 281
    Width = 150
    Height = 57
    Cursor = crHandPoint
    Caption = 'Adaug'#259
    Font.Charset = ANSI_CHARSET
    Font.Color = 14337153
    Font.Height = -27
    Font.Name = 'Ubuntu'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
    OnClick = addBClick
  end
  object le: TLabeledEdit
    Left = 192
    Top = 84
    Width = 281
    Height = 33
    EditLabel.Width = 70
    EditLabel.Height = 20
    EditLabel.Caption = 'Localitate'
    Font.Charset = ANSI_CHARSET
    Font.Color = 4339219
    Font.Height = -20
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    OnChange = leChange
  end
  object Acb: TComboBox
    Left = 488
    Top = 84
    Width = 185
    Height = 34
    Style = csDropDownList
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -20
    Font.Name = 'Ubuntu Light'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 7
  end
  object mem: TMemo
    Left = 192
    Top = 170
    Width = 661
    Height = 231
    Font.Charset = ANSI_CHARSET
    Font.Color = 4339219
    Font.Height = -16
    Font.Name = 'Ubuntu Mono'
    Font.Style = []
    Lines.Strings = (
      'mem')
    ParentFont = False
    ScrollBars = ssBoth
    TabOrder = 8
    OnChange = memChange
    OnKeyDown = memKeyDown
  end
  object nameCaseB: TButton
    Left = 455
    Top = 64
    Width = 18
    Height = 18
    Caption = '*'
    TabOrder = 9
    OnClick = nameCaseBClick
  end
end
