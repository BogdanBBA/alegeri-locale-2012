﻿unit EdCoordO;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls;

type
  TFEdCO = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    lb: TListBox;
    closeB: TButton;
    saveB: TButton;
    saveT: TTimer;
    Label6: TLabel;
    Label7: TLabel;
    ch1: TCheckBox;
    Label8: TLabel;
    Label9: TLabel;
    rb1: TRadioButton;
    rb2: TRadioButton;
    Label10: TLabel;
    rb3: TRadioButton;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    procedure closeBClick(Sender: TObject);
    procedure saveBClick(Sender: TObject);
    procedure saveTTimer(Sender: TObject);
    procedure lbClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FEdCO: TFEdCO;

implementation

{$R *.dfm}

uses DataTypes, functii, AL12_pas;

procedure TFEdCO.closeBClick(Sender: TObject);
begin F1.refBClick(closeB); FEdCO.Close end;

procedure TFEdCO.FormShow(Sender: TObject);
var i: word;
begin
  if noc=0 then begin showmessage('Nu sunt orase de editat.'); closeBClick(FEdCO); exit end;
  Label13.Caption:='Orașe: '+inttostr(noc); lb.Clear; for i:=0 to noc-1 do lb.Items.Add(oc[i].Nume); lb.ItemIndex:=0; lbClick(FEdCO)
end;

procedure TFEdCO.lbClick(Sender: TObject);
var i: word;
begin
  F1.OnResize(lb);
  for i:=0 to noc-1 do F1.ccVisibility(lb, i, (i<>lb.ItemIndex) and (not ch1.Checked));
  Label3.Caption:='"'+oc[lb.ItemIndex].Nume+'"';
  Label7.Caption:='"'+oc[lb.ItemIndex].JudetIfOras+'"';
  Label5.Caption:=inttostr(oc[lb.ItemIndex].X)+', '+inttostr(oc[lb.ItemIndex].Y);
  TRadioButton(FindComponent('rb'+inttostr(oc[lb.ItemIndex].CSizeIfOras))).Checked:=true
end;

procedure TFEdCO.saveBClick(Sender: TObject);
begin
  if rb1.Checked then oc[lb.ItemIndex].CSizeIfOras:=1 else if rb2.Checked then oc[lb.ItemIndex].CSizeIfOras:=2 else oc[lb.ItemIndex].CSizeIfOras:=3;
  cc[lb.ItemIndex].Width:=CCSize[oc[lb.ItemIndex].CSizeIfOras]; cc[lb.ItemIndex].Height:=cc[lb.ItemIndex].Width; lbClick(saveB);
  SaveDatatoXml('coord'); saveB.Caption:='OK'; saveB.Enabled:=false; saveT.Enabled:=true
end;

procedure TFEdCO.saveTTimer(Sender: TObject);
begin
  saveT.Enabled:=false; saveB.Caption:='Salvare coord.'; saveB.Enabled:=true
end;

end.
