﻿unit AdRezNoi;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, XML.VerySimple;

type
  TFAddRA = class(TForm)
    locL: TLabel;
    rb1: TRadioButton;
    Label1: TLabel;
    rb2: TRadioButton;
    Label2: TLabel;
    rb3: TRadioButton;
    Label3: TLabel;
    rb4: TRadioButton;
    Label4: TLabel;
    closeB: TButton;
    addB: TButton;
    le: TLabeledEdit;
    Acb: TComboBox;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    mem: TMemo;
    Label8: TLabel;
    nameCaseB: TButton;
    procedure closeBClick(Sender: TObject);
    procedure addBClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure leChange(Sender: TObject);
    procedure rb1Click(Sender: TObject);
    procedure memChange(Sender: TObject);
    procedure memKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure nameCaseBClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FAddRA: TFAddRA;

implementation

uses AL12_pas, DataTypes, functii, EdCoordO;

{$R *.dfm}

procedure TFAddRA.addBClick(Sender: TObject);
var i, j, p: word; s: string; b, addedO: boolean; ax: TVotare; x: TverySimpleXML;
begin if Label6.Visible then begin showmessage('orasul exista deja, deocamdata nu e suportata functia de supra-scriere'); exit end;
  //check lines
  s:=mem.Text; clean_str(s, true, true); mem.Text:=s;
  for i:=0 to mem.Lines.Count-1 do
    begin
      b:=false; p:=0;
      for j:=0 to nfp-1 do
        begin
          p:=pos(ansiuppercase(fp[j].NumeDinListeOficial), ansiuppercase(MultiStringReplace(mem.Lines[i], ['Ă', 'Â', 'Î', 'Ș', 'Ț', 'Ş', 'Ţ'], ['A', 'A', 'I', 'S', 'T', 'S', 'T'], [rfReplaceAll, rfIgnoreCase])));
          if p<>0 then begin b:=true; break end
        end;
      if b and (p<>1) then begin if MessageDlg('There is something wrong with the following line (the position of the political party = '+inttostr(p)+').'+dnl+'"'+mem.Lines[i]+'"'+dnl+'Continue?', mtWarning, mbOKCancel, 0)<>mrOK then exit end
      else if not b then begin case MessageDlg('I can not find the name of a political party in the following line.'+dnl+'"'+mem.Lines[i]+'"'+dnl+'It could be that you need to add the party to the database. If you don''t add it, errors may occur. Add it now?', mtError, [mbYes, mbNo, mbAbort], 0) of mrYes: begin showmessage('OK. Be sure to fill in the "Nume oficial" information.'); F1.Adugareformaiunepolitic1Click(addB); exit end; mrAbort: exit end end
    end;
  //optional: add city and county
  nameCaseBClick(addB); addedO:=false;
  if Label7.Visible then
    begin
      if pos(', ', le.Text)=0 then begin showmessage('You need to include ", " in the name of the city to separate it from the county (format: "City, County").'); exit end;
      addedO:=true; inc(noc); setlength(oc, noc);
      with oc[noc-1] do begin Nume:=le.Text; JudetIfOras:=copy(le.Text, pos(', ', le.Text)+2, length(le.Text)); CSizeIfOras:=2; X:=0; Y:=0 end;
      if jl.IndexOf(oc[noc-1].JudetIfOras)=-1 then
        begin inc(njc); setlength(jc, njc); with jc[njc-1] do begin Nume:=oc[noc-1].JudetIfOras; X:=0; Y:=0 end end; //showmessage(oc[noc-1].Nume+' ('+oc[noc-1].JudetIfOras+')'+nl+jc[njc-1].Nume);
      SaveDatatoXML('coord')
    end;
  //decode candidates
  try
    ax.Year:=strtoint(Acb.Text); ax.Unde:=le.Text; ax.Functie:='P'; ax.Candidati.nc:=mem.Lines.Count; setlength(ax.Candidati.Candidat, ax.Candidati.nc);
    for i:=0 to mem.Lines.Count-1 do
      begin
        s:=mem.Lines[i]; p:=0;
        for j:=0 to nfp-1 do if pos(ansiuppercase(fp[j].NumeDinListeOficial), ansiuppercase(MultiStringReplace(s, ['Ă', 'Â', 'Î', 'Ș', 'Ț', 'Ş', 'Ţ'], ['A', 'A', 'I', 'S', 'T', 'S', 'T'], [rfReplaceAll, rfIgnoreCase])))<>0 then begin p:=j; break end;
        with ax.Candidati.Candidat[i] do
          begin
            Politica := fp[p].NumeScurt;
              delete(s, 1, length(fp[p].NumeDinListeOficial)+1); while charinset(s[length(s)], ['%', ' ']) do delete(s, length(s), 1); p:=LastSpace(s);
            Procentaj := strtofloat(copy(s, p, 7));
              delete(s, p, 7); p:=LastSpace(s);
            NrVoturi := strtoint(copy(s, p, 7));
              delete(s, p, 7);
            Nume:=s;
            log('Decoded Nume: '+Nume+', Politica: '+Politica+', NrVoturi: '+inttostr(NrVoturi)+', Procentaj: '+floattostr(Procentaj)+' from line i = '+inttostr(i)+' = "'+mem.Lines[i]+'"')
          end;
      end;
    inc(nvot); setlength(vot, nvot); vot[nvot-1]:=ax; SaveDatatoXML('alegeri');
  except on E:Exception do begin showmessage('An error has occured whil decoding...'+nl+'...at line I='+inttostr(i)+dnl+E.ClassName+': '+E.Message); exit end end;
  //
  MessageDlg('Operation successful!', mtInformation, [mbOk], 0);
  //
  if addedO then if MessageDlg('You''ve added a new city. Would you like to edit its coordonates now?', mtConfirmation, mbYesNo, 0)=mrYes then
    begin x:=TverySimpleXML.Create; x.Root.NodeName:='Restart'; x.Root.SetAttribute('when', datetimetostr(now)); x.Root.SetAttribute('edit', 'orase');
      x.Root.SetAttribute('what', oc[noc-1].Nume); x.SaveToFile('data\restart.xml'); x.Free; F1.Restartprogram1Click(addB) end
end;

procedure TFAddRA.closeBClick(Sender: TObject);
begin
  FAddRA.Close; F1.refBClick(FAddRA)
end;

procedure TFAddRA.FormShow(Sender: TObject);
begin
  rb1Click(FAddRA); le.text:=''; Acb.Items:=F1.Acb.Items; Acb.ItemIndex:=Acb.Items.Count-1; mem.Clear; memChange(FAddRA)
end;

procedure TFAddRA.leChange(Sender: TObject);
begin
  Label6.Visible:=false; Label7.Visible:=false; if le.Text='' then exit;
  if ol.IndexOf(le.Text)=-1 then Label7.Visible:=true else Label6.Visible:=true
end;

procedure TFAddRA.memChange(Sender: TObject);
begin
  addB.Enabled:=(mem.Text<>'') and (le.Text<>'')
end;

procedure TFAddRA.memKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (chr(key)='A') and (ssCtrl in Shift) then mem.SelectAll
end;

procedure TFAddRA.nameCaseBClick(Sender: TObject);
begin le.Text:=RightCase(le.Text); leChange(nameCaseB) end;

procedure TFAddRA.rb1Click(Sender: TObject);
begin
  le.EditLabel.Caption:='Localitate, Județ';
end;

end.
