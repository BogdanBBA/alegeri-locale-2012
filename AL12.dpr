program AL12;

uses
  Forms,
  AL12_pas in 'AL12_pas.pas' {F1},
  DataTypes in 'DataTypes.pas',
  EdCoordJ in 'EdCoordJ.pas' {FEdCJ},
  EdCoordO in 'EdCoordO.pas' {FEdCO},
  AdRezNoi in 'AdRezNoi.pas' {FAddRA},
  EditorFP in 'EditorFP.pas' {FEdFP},
  RezLoc in 'RezLoc.pas' {FRL},
  RezFP in 'RezFP.pas' {FRFP},
  RezCand in 'RezCand.pas' {FRCand},
  SetSexCand in 'SetSexCand.pas' {FSSC};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TF1, F1);
  Application.CreateForm(TFEdCJ, FEdCJ);
  Application.CreateForm(TFEdCO, FEdCO);
  Application.CreateForm(TFAddRA, FAddRA);
  Application.CreateForm(TFEdFP, FEdFP);
  Application.CreateForm(TFRL, FRL);
  Application.CreateForm(TFRFP, FRFP);
  Application.CreateForm(TFRCand, FRCand);
  Application.CreateForm(TFSSC, FSSC);
  Application.Run;
end.
