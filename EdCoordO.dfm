object FEdCO: TFEdCO
  Left = 0
  Top = 0
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsSingle
  Caption = 'Editor Coordonate Ora'#537'e'
  ClientHeight = 426
  ClientWidth = 442
  Color = clBlack
  Font.Charset = ANSI_CHARSET
  Font.Color = clWhite
  Font.Height = -17
  Font.Name = 'Ubuntu'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  ScreenSnap = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 21
  object Label1: TLabel
    Left = 16
    Top = 8
    Width = 396
    Height = 21
    Caption = 'Alege un ora'#537', apoi d'#259' click pe hart'#259' pentru a-l fixa.'
  end
  object Label2: TLabel
    Left = 223
    Top = 48
    Width = 35
    Height = 22
    Caption = 'Ora'#537
    Font.Charset = ANSI_CHARSET
    Font.Color = clSilver
    Font.Height = -17
    Font.Name = 'Ubuntu'
    Font.Style = [fsItalic]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 223
    Top = 67
    Width = 52
    Height = 21
    Caption = 'Label3'
  end
  object Label4: TLabel
    Left = 223
    Top = 140
    Width = 86
    Height = 22
    Caption = 'Coordonate'
    Font.Charset = ANSI_CHARSET
    Font.Color = clSilver
    Font.Height = -17
    Font.Name = 'Ubuntu'
    Font.Style = [fsItalic]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 223
    Top = 159
    Width = 52
    Height = 21
    Caption = 'Label5'
  end
  object Label6: TLabel
    Left = 223
    Top = 94
    Width = 42
    Height = 22
    Caption = 'Jude'#539
    Font.Charset = ANSI_CHARSET
    Font.Color = clSilver
    Font.Height = -17
    Font.Name = 'Ubuntu'
    Font.Style = [fsItalic]
    ParentFont = False
  end
  object Label7: TLabel
    Left = 223
    Top = 113
    Width = 52
    Height = 21
    Caption = 'Label7'
  end
  object Label8: TLabel
    Left = 240
    Top = 288
    Width = 165
    Height = 19
    Caption = 'Ascunde restul ora'#537'elor'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -15
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
  end
  object Label9: TLabel
    Left = 239
    Top = 212
    Width = 73
    Height = 21
    Caption = 'Municipiu'
  end
  object Label10: TLabel
    Left = 239
    Top = 233
    Width = 37
    Height = 21
    Caption = 'Ora'#537'e'
  end
  object Label11: TLabel
    Left = 239
    Top = 253
    Width = 66
    Height = 21
    Caption = 'Ora'#537'ele'
  end
  object Label12: TLabel
    Left = 223
    Top = 186
    Width = 56
    Height = 22
    Caption = 'M'#259'rime'
    Font.Charset = ANSI_CHARSET
    Font.Color = clSilver
    Font.Height = -17
    Font.Name = 'Ubuntu'
    Font.Style = [fsItalic]
    ParentFont = False
  end
  object Label13: TLabel
    Left = 16
    Top = 402
    Width = 60
    Height = 22
    Caption = 'Label13'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -17
    Font.Name = 'Ubuntu'
    Font.Style = [fsItalic]
    ParentFont = False
  end
  object lb: TListBox
    Left = 16
    Top = 48
    Width = 201
    Height = 353
    Font.Charset = ANSI_CHARSET
    Font.Color = clGray
    Font.Height = -19
    Font.Name = 'Ubuntu'
    Font.Style = []
    ItemHeight = 24
    ParentFont = False
    TabOrder = 0
    OnClick = lbClick
  end
  object closeB: TButton
    Left = 223
    Top = 360
    Width = 195
    Height = 41
    Caption = #206'nchide'
    Font.Charset = ANSI_CHARSET
    Font.Color = clSilver
    Font.Height = -23
    Font.Name = 'Ubuntu'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = closeBClick
  end
  object saveB: TButton
    Left = 223
    Top = 313
    Width = 195
    Height = 41
    Caption = 'Salvare coord.'
    Font.Charset = ANSI_CHARSET
    Font.Color = clSilver
    Font.Height = -23
    Font.Name = 'Ubuntu'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    OnClick = saveBClick
  end
  object ch1: TCheckBox
    Left = 223
    Top = 290
    Width = 195
    Height = 17
    TabOrder = 3
    OnClick = lbClick
  end
  object rb1: TRadioButton
    Left = 223
    Top = 214
    Width = 150
    Height = 17
    TabOrder = 4
  end
  object rb2: TRadioButton
    Left = 223
    Top = 235
    Width = 150
    Height = 17
    TabOrder = 5
  end
  object rb3: TRadioButton
    Left = 223
    Top = 256
    Width = 150
    Height = 17
    Checked = True
    TabOrder = 6
    TabStop = True
  end
  object saveT: TTimer
    Enabled = False
    Interval = 2000
    OnTimer = saveTTimer
    Left = 384
    Top = 312
  end
end
