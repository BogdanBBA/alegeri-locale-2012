object FEdFP: TFEdFP
  Left = 0
  Top = 0
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsSingle
  Caption = 'Editare Forma'#539'iuni Politice'
  ClientHeight = 443
  ClientWidth = 623
  Color = clBlack
  Font.Charset = ANSI_CHARSET
  Font.Color = clWhite
  Font.Height = -16
  Font.Name = 'Ubuntu'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  ScreenSnap = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 20
  object locL: TLabel
    Left = 16
    Top = 8
    Width = 439
    Height = 34
    Caption = 'Editeaz'#259' datele '#537'i apas'#259' '#8221'Salvare'#8221
    Font.Charset = ANSI_CHARSET
    Font.Color = 14337153
    Font.Height = -29
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel
    Left = 232
    Top = 218
    Width = 57
    Height = 20
    Caption = 'Culoare'
  end
  object IDErrSH: TShape
    Left = 583
    Top = 41
    Width = 25
    Height = 25
    Brush.Color = clRed
    Shape = stCircle
  end
  object colSH: TShape
    Left = 343
    Top = 224
    Width = 265
    Height = 45
  end
  object Label13: TLabel
    Left = 16
    Top = 420
    Width = 60
    Height = 22
    Caption = 'Label13'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -17
    Font.Name = 'Ubuntu'
    Font.Style = [fsItalic]
    ParentFont = False
  end
  object exitB: TButton
    Left = 423
    Top = 321
    Width = 185
    Height = 57
    Cursor = crHandPoint
    Caption = #206'nchide'
    Font.Charset = ANSI_CHARSET
    Font.Color = clSilver
    Font.Height = -27
    Font.Name = 'Ubuntu'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    OnClick = exitBClick
  end
  object Button1: TButton
    Left = 232
    Top = 321
    Width = 185
    Height = 57
    Cursor = crHandPoint
    Caption = 'Salvare'
    Font.Charset = ANSI_CHARSET
    Font.Color = clSilver
    Font.Height = -27
    Font.Name = 'Ubuntu'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = Button1Click
  end
  object lb: TListBox
    Left = 16
    Top = 48
    Width = 185
    Height = 370
    Font.Charset = ANSI_CHARSET
    Font.Color = 4339219
    Font.Height = -16
    Font.Name = 'Ubuntu'
    Font.Style = []
    ItemHeight = 20
    ParentFont = False
    TabOrder = 2
    OnClick = lbClick
  end
  object le1: TLabeledEdit
    Left = 232
    Top = 72
    Width = 376
    Height = 28
    EditLabel.Width = 216
    EditLabel.Height = 20
    EditLabel.Caption = 'Nume scurt (unic, folosit ca ID)'
    Font.Charset = ANSI_CHARSET
    Font.Color = 4339219
    Font.Height = -16
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    Text = 'text'
    OnChange = le1Change
  end
  object le2: TLabeledEdit
    Left = 232
    Top = 128
    Width = 376
    Height = 28
    EditLabel.Width = 109
    EditLabel.Height = 20
    EditLabel.Caption = 'Nume complet '
    Font.Charset = ANSI_CHARSET
    Font.Color = 4339219
    Font.Height = -16
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    Text = 'text'
  end
  object le3: TLabeledEdit
    Left = 232
    Top = 184
    Width = 376
    Height = 28
    EditLabel.Width = 387
    EditLabel.Height = 20
    EditLabel.Caption = 'Nume oficial (forma din listele oficiale ale rezultatelor)'
    Font.Charset = ANSI_CHARSET
    Font.Color = 4339219
    Font.Height = -16
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    Text = 'text'
  end
  object Button2: TButton
    Left = 232
    Top = 244
    Width = 105
    Height = 25
    Caption = 'Modific'#259
    TabOrder = 6
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 423
    Top = 384
    Width = 185
    Height = 34
    Caption = #536'tergere selec'#539'ie'
    Font.Charset = ANSI_CHARSET
    Font.Color = clSilver
    Font.Height = -21
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 232
    Top = 384
    Width = 185
    Height = 34
    Caption = 'Forma'#539'iune nou'#259
    Font.Charset = ANSI_CHARSET
    Font.Color = clSilver
    Font.Height = -21
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
    TabOrder = 8
    OnClick = Button4Click
  end
  object cld: TColorDialog
    Options = [cdFullOpen, cdShowHelp, cdSolidColor, cdAnyColor]
    Left = 304
    Top = 272
  end
  object saveT: TTimer
    Interval = 2000
    OnTimer = saveTTimer
    Left = 384
    Top = 280
  end
end
