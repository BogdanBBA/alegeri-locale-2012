﻿unit RezLoc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, TeEngine, Series, ExtCtrls, TeeProcs, Chart, Menus;

type
  TFRL = class(TForm)
    closeB: TButton;
    Acb: TComboBox;
    Lcb: TComboBox;
    Fcb: TComboBox;
    ch: TChart;
    Series1: TPieSeries;
    Label1: TLabel;
    rb1: TRadioButton;
    Label2: TLabel;
    rb2: TRadioButton;
    Series2: THorizBarSeries;
    Label3: TLabel;
    rb3: TRadioButton;
    pm1: TPopupMenu;
    plusCandB: TMenuItem;
    plusFPB: TMenuItem;
    procedure FormShow(Sender: TObject);
    procedure LcbChange(Sender: TObject);
    procedure closeBClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure chClickSeries(Sender: TCustomChart; Series: TChartSeries;
      ValueIndex: Integer; Button: TMouseButton; Shift: TShiftState; X,
      Y: Integer);
    procedure plusCandBClick(Sender: TObject);
    procedure plusFPBClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRL: TFRL;

implementation

uses AL12_pas, functii, DataTypes, RezCand, RezFP;

{$R *.dfm}

procedure TFRL.chClickSeries(Sender: TCustomChart; Series: TChartSeries;
  ValueIndex: Integer; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var p: TPoint; sx: string; i, j: integer;
begin
  sx:=Series.Labels[ValueIndex]; sx:=copy(sx, 1, pos(' (', sx)-1); FCandidat(sx, i, j);
  pm1.Items[0].Caption:='Mai multe informații despre candidat ('+vot[i].Candidati.Candidat[j].Nume+')';
  pm1.Items[1].Caption:='Mai multe informații despre formațiunea politică ('+vot[i].Candidati.Candidat[j].Politica+')';
  getcursorpos(p); pm1.Popup(p.x, p.y)
end;

procedure TFRL.closeBClick(Sender: TObject);
begin
  FRL.Close
end;

procedure TFRL.FormResize(Sender: TObject);
begin
  closeB.Left:=FRL.Width-closeB.Width-31;
  ch.Width:=closeB.Left-ch.Left+closeB.Width; ch.Height:=FRL.Height-131
end;

procedure TFRL.FormShow(Sender: TObject);
var i: word;
begin
  Acb.Items:=F1.Acb.Items; Acb.ItemIndex:=F1.Acb.ItemIndex;
  for i:=0 to noc-1 do Lcb.Items.Add(oc[i].Nume); Lcb.ItemIndex:=Lcb.Items.IndexOf(locShow);
  LcbChange(FRL)
end;

procedure TFRL.LcbChange(Sender: TObject);
  {*}function plr(x: word): string; begin if x=1 then result:='candidat' else result:='candidați' end;
var x, i: integer; fct: string; p: TPieSeries; b: TBarSeries; h: THorizBarSeries;
begin
  fct:=Functie(Fcb.ItemIndex+1, false);
  x:=FVotare(a[Acb.ItemIndex], fct, Lcb.Text); if x=-1 then begin showmessage('Nu am informații despre alegerile pe care le-ai selectat !'); exit end;
  ch.ClearChart; ch.View3D:=false;
  ch.Title.Font.Name:='Ubuntu'; ch.Title.Font.Style:=[fsBold]; ch.Title.Font.Size:=13; ch.Title.Font.Color:=$00522914;
  ch.SubTitle.Font:=ch.Title.Font; ch.SubTitle.Font.Size:=10; ch.SubTitle.Font.Color:=$007D532D;
  ch.Title.Caption:='Rezultate alegeri în '+Lcb.Text+' pentru '+Fcb.Text+' ('+Acb.Text+')';
  ch.SubTitle.Caption:='Afișez '+inttostr(vot[x].Candidati.nc)+' '+plr(vot[x].Candidati.nc)+' ce au primit un total de '+grupat(vot[x].TotalVoturi)+' voturi';
  //
  if rb1.Checked then
    begin
      p:=TPieSeries.Create(Self); p.Active:=true; p.Circled:=true;
      for i:=0 to vot[x].Candidati.nc-1 do
        p.AddPie(vot[x].Candidati.Candidat[i].NrVoturi, vot[x].Candidati.Candidat[i].Nume+' ('+vot[x].Candidati.Candidat[i].Politica+') ['+floattostr(vot[x].Candidati.Candidat[i].Procentaj)+'%]', fp[FPozPartidinFP(vot[x].Candidati.Candidat[i].Politica)].Culoare);
      ch.AddSeries(p)
    end
  else if rb2.Checked then
    begin
      b:=TBarSeries.Create(Self); b.Active:=true;
      for i:=0 to vot[x].Candidati.nc-1 do
        b.AddBar(vot[x].Candidati.Candidat[i].NrVoturi, vot[x].Candidati.Candidat[i].Nume+' ('+vot[x].Candidati.Candidat[i].Politica+') ['+floattostr(vot[x].Candidati.Candidat[i].Procentaj)+'%]', fp[FPozPartidinFP(vot[x].Candidati.Candidat[i].Politica)].Culoare);
      ch.AddSeries(b)
    end
  else if rb3.Checked then
    begin
      h:=THorizBarSeries.Create(Self); h.Active:=true;
      for i:=0 to vot[x].Candidati.nc-1 do
        h.AddBar(vot[x].Candidati.Candidat[i].NrVoturi, vot[x].Candidati.Candidat[i].Nume+' ('+vot[x].Candidati.Candidat[i].Politica+') ['+floattostr(vot[x].Candidati.Candidat[i].Procentaj)+'%]', fp[FPozPartidinFP(vot[x].Candidati.Candidat[i].Politica)].Culoare);
      ch.AddSeries(h)
    end;
end;

procedure TFRL.plusCandBClick(Sender: TObject);
begin candShow:=copy(plusCandB.Caption, pos('(', plusCandB.Caption)+1, pos(')', plusCandB.Caption)-pos('(', plusCandB.Caption)-1); if FRCand.showing then FRCand.OnShow(plusCandB) else FRCand.show; FRCand.SetFocus end;

procedure TFRL.plusFPBClick(Sender: TObject);
begin fpShow:=copy(plusFPB.Caption, pos('(', plusFPB.Caption)+1, pos(')', plusFPB.Caption)-pos('(', plusFPB.Caption)-1); if FRFP.Showing then FRFP.OnShow(plusFPB) else FRFP.Show; FRFP.SetFocus end;

end.
