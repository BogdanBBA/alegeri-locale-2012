﻿unit SetSexCand;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TFSSC = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    mem: TMemo;
    closeB: TButton;
    Button1: TButton;
    Label3: TLabel;
    Label4: TLabel;
    procedure closeBClick(Sender: TObject);
    procedure memChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FSSC: TFSSC;

implementation

{$R *.dfm}

uses AL12_pas, DataTypes;

procedure TFSSC.Button1Click(Sender: TObject);
var i, j, k, kk: word;
begin
  for i:=0 to nvot-1 do
    for j:=0 to vot[i].Candidati.nc-1 do
      for k:=0 to mem.Lines.Count-1 do
        if pos(ansiuppercase(mem.Lines[k]), ansiuppercase(vot[i].Candidati.Candidat[j].Nume))<>0 then
          vot[i].Candidati.Candidat[j].Sex:='F';
  k:=0; kk:=0;
  for i:=0 to nvot-1 do for j:=0 to vot[i].Candidati.nc-1 do
    if vot[i].Candidati.Candidat[j].Sex='F' then inc(k) else inc(kk);
  showmessage('Succes!'+dnl+'Există '+inttostr(k+kk)+' candidați în baza de date, din care '+inttostr(k)+' femei și '+inttostr(kk)+' bărbați.')
end;

procedure TFSSC.closeBClick(Sender: TObject);
begin
  FSSC.Close
end;

procedure TFSSC.FormShow(Sender: TObject);
begin
  memChange(FSSC)
end;

procedure TFSSC.memChange(Sender: TObject);
begin
  Label3.Caption:=inttostr(mem.lines.count)+' nume'
end;

end.
