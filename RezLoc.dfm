object FRL: TFRL
  Left = 0
  Top = 0
  BorderIcons = [biMinimize, biMaximize]
  Caption = 'Rezultate alegeri localitate'
  ClientHeight = 465
  ClientWidth = 805
  Color = clBlack
  Constraints.MinHeight = 503
  Constraints.MinWidth = 821
  Font.Charset = ANSI_CHARSET
  Font.Color = clWhite
  Font.Height = -19
  Font.Name = 'Ubuntu'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 24
  object Label1: TLabel
    Left = 32
    Top = 54
    Width = 63
    Height = 20
    Caption = 'Pie chart'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -16
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 176
    Top = 53
    Width = 65
    Height = 20
    Caption = 'Bar chart'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -16
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 320
    Top = 53
    Width = 143
    Height = 20
    Caption = 'Horizontal bar chart'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -16
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
  end
  object closeB: TButton
    Left = 640
    Top = 15
    Width = 150
    Height = 57
    Caption = #206'nchide'
    Font.Charset = ANSI_CHARSET
    Font.Color = 14337153
    Font.Height = -27
    Font.Name = 'Ubuntu'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    OnClick = closeBClick
  end
  object Acb: TComboBox
    Left = 520
    Top = 15
    Width = 114
    Height = 35
    Style = csDropDownList
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -21
    Font.Name = 'Ubuntu Light'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnChange = LcbChange
  end
  object Lcb: TComboBox
    Left = 160
    Top = 15
    Width = 354
    Height = 33
    Style = csDropDownList
    DropDownCount = 20
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -21
    Font.Name = 'Ubuntu Light'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    OnChange = LcbChange
  end
  object Fcb: TComboBox
    Left = 8
    Top = 15
    Width = 146
    Height = 35
    Style = csDropDownList
    Enabled = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -21
    Font.Name = 'Ubuntu Light'
    Font.Style = [fsBold]
    ItemIndex = 0
    ParentFont = False
    TabOrder = 3
    Text = 'Primar'
    Items.Strings = (
      'Primar'
      'Pre'#537'edinte CJ'
      'Consiliul Local'
      'Consiliul Jude'#539'ean')
  end
  object ch: TChart
    Left = 16
    Top = 78
    Width = 774
    Height = 371
    Title.Text.Strings = (
      'TChart')
    OnClickSeries = chClickSeries
    View3D = False
    View3DOptions.Elevation = 315
    View3DOptions.Orthogonal = False
    View3DOptions.Perspective = 0
    View3DOptions.Rotation = 360
    Color = 9079434
    TabOrder = 4
    ColorPaletteIndex = 13
    object Series1: TPieSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = True
      Circled = True
      Gradient.Direction = gdRadial
      OtherSlice.Legend.Visible = False
      PieValues.Name = 'Pie'
      PieValues.Order = loNone
    end
    object Series2: THorizBarSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = True
      Gradient.Direction = gdLeftRight
      Shadow.Color = 8487297
      XValues.Name = 'Bar'
      XValues.Order = loNone
      YValues.Name = 'Y'
      YValues.Order = loAscending
    end
  end
  object rb1: TRadioButton
    Left = 16
    Top = 56
    Width = 138
    Height = 17
    Checked = True
    TabOrder = 5
    TabStop = True
    OnClick = LcbChange
  end
  object rb2: TRadioButton
    Left = 160
    Top = 55
    Width = 138
    Height = 17
    TabOrder = 6
    OnClick = LcbChange
  end
  object rb3: TRadioButton
    Left = 304
    Top = 55
    Width = 159
    Height = 17
    TabOrder = 7
    OnClick = LcbChange
  end
  object pm1: TPopupMenu
    Left = 720
    Top = 192
    object plusCandB: TMenuItem
      Caption = 'Mai multe informa'#539'ii despre candidat'
      OnClick = plusCandBClick
    end
    object plusFPB: TMenuItem
      Caption = 'Mai multe informa'#539'ii despre  forma'#539'iunea politic'#259
      OnClick = plusFPBClick
    end
  end
end
