﻿unit AL12_pas;

// V1.0 BETA - June 11th/12th, 2012

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, XML.VerySimple, StdCtrls, pngimage, ExtCtrls, Menus, jpeg, DateUtils,
  ShellAPI, ComCtrls;

type
  TF1 = class(TForm)
    img: TImage;
    exitB: TButton;
    p0: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    optB: TButton;
    optM: TPopupMenu;
    Editarecoordonate1: TMenuItem;
    Acb: TComboBox;
    refB: TButton;
    logL: TLabel;
    TestL: TLabel;
    Shape1: TShape;
    Shape2: TShape;
    Shape3: TShape;
    Editarecoordonate2: TMenuItem;
    Panel1: TPanel;
    locL: TLabel;
    alesImg: TImage;
    alesL: TLabel;
    polAL: TLabel;
    polACImg: TImage;
    p1: TPanel;
    candImg: TImage;
    candL: TLabel;
    polCandL: TLabel;
    procCandL: TLabel;
    candPolColImg: TImage;
    functieL: TLabel;
    Label3: TLabel;
    nrVotL: TLabel;
    alesProcL: TLabel;
    Editarerezultatealegeri1: TMenuItem;
    Adugarealegerinoi1: TMenuItem;
    Editareformaiunipolitice1: TMenuItem;
    Adugareformaiunepolitic1: TMenuItem;
    Restartprogram1: TMenuItem;
    Openlogfile1: TMenuItem;
    Closeprogram1: TMenuItem;
    Gndaci1: TMenuItem;
    Despre1: TMenuItem;
    Deschidereraportdelapornire1: TMenuItem;
    Vizualizarerezultatepeformaiuni1: TMenuItem;
    Vizualizarerezultatepecandidai1: TMenuItem;
    Vizualizarerezultatepelocaii1: TMenuItem;
    Setaresexcandidate1: TMenuItem;
    appb: TProgressBar;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure exitBClick(Sender: TObject);
    procedure imgMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure Editarecoordonate1Click(Sender: TObject);
    procedure Editarecoordonate2Click(Sender: TObject);
    procedure imgClick(Sender: TObject);
    procedure refBClick(Sender: TObject);
    procedure lgVisibility(Sender: TObject; x: word; viz: boolean);
    procedure ccVisibility(Sender: TObject; x: word; viz: boolean);
    procedure MouseOverCity(Sender: TObject);
    procedure CandPanelResize(Sender: TObject);
    procedure Adugarealegerinoi1Click(Sender: TObject);
    procedure Editareformaiunipolitice1Click(Sender: TObject);
    procedure Adugareformaiunepolitic1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Restartprogram1Click(Sender: TObject);
    procedure Openlogfile1Click(Sender: TObject);
    procedure optBClick(Sender: TObject);
    procedure Gndaci1Click(Sender: TObject);
    procedure Despre1Click(Sender: TObject);
    procedure locLClick(Sender: TObject);
    procedure Deschidereraportdelapornire1Click(Sender: TObject);
    procedure polALClick(Sender: TObject);
    procedure Vizualizarerezultatepeformaiuni1Click(Sender: TObject);
    procedure alesLClick(Sender: TObject);
    procedure Vizualizarerezultatepelocaii1Click(Sender: TObject);
    procedure Vizualizarerezultatepecandidai1Click(Sender: TObject);
    procedure Setaresexcandidate1Click(Sender: TObject);
    procedure appbMouseEnter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F1: TF1;

implementation

{$R *.dfm}

uses DataTypes, functii, EdCoordJ, EdCoordO, AdRezNoi, EditorFP, RezLoc, RezFP,
  RezCand, SetSexCand;

procedure TF1.Editarecoordonate1Click(Sender: TObject);
begin
  if FEdCO.Showing then begin showmessage('Închide întâi fereastra de editare a coordonatelor orașelor.'); exit end;
  if FEdCJ.Showing then FEdCJ.OnShow(Editarecoordonate1) else FEdCJ.Show
end;

procedure TF1.Editarecoordonate2Click(Sender: TObject);
begin
  if FEdCJ.Showing then begin showmessage('Închide întâi fereastra de editare a coordonatelor județelor.'); exit end;
  if FEdCO.Showing then FEdCO.OnShow(Editarecoordonate2) else FEdCO.Show
end;

procedure TF1.Editareformaiunipolitice1Click(Sender: TObject);
begin
  FEdFP.Show
end;

procedure TF1.exitBClick(Sender: TObject);
begin
  Halt
end;

procedure TF1.FormCreate(Sender: TObject);
var i: word;
begin
  DateSeparator:='/'; TimeSeparator:=':'; DecimalSeparator:='.'; ThousandSeparator:=',';
  ShortDateFormat:='dd/mm/yyyy'; LongTimeFormat:='hh:nn:ss';
  //
  if (Screen.Width<1360) or (Screen.Height<750) then showmessage('Your screen is too small! The program will run, but you probably won''t see much.');
  //
  ReadData; GenerateTextReport(false);
  //
  setlength(lg, njc);
  for i:=0 to njc-1 do
    begin
      lg[i].nameL:=TLabel.Create(Self); lg[i].nameL.Parent:=F1; lg[i].nameL.Name:='lg_'+inttostr(i)+'_nameL'; lg[i].nameL.Font:=Label1.Font;
      lg[i].img:=TImage.Create(Self); lg[i].img.Parent:=F1; lg[i].img.Name:='lg_'+inttostr(i)+'_img';
      lg[i].img.Height:=lg[i].nameL.Height div 2
    end;
  //
  setlength(cc, noc);
  for i:=0 to noc-1 do
    begin
      cc[i]:=TShape.Create(Self); cc[i].Parent:=F1;
      cc[i].Name:='cc_'+inttostr(i); cc[i].OnMouseEnter:=MouseOverCity;
      cc[i].Shape:=stCircle; cc[i].Width:=CCSize[oc[i].CSizeIfOras]; cc[i].Height:=cc[i].Width
    end;
  //
  setlength(cg, ncg);
  for i:=0 to ncg-1 do
    begin
      cg[i].p:=TPanel.Create(Self); cg[i].p.Parent:=Panel1; cg[i].p.Name:='cg_'+inttostr(i)+'_p'; cg[i].p.Caption:='';
        cg[i].p.BevelOuter:=bvNone; cg[i].p.Top:=p1.Top+i*(p1.Height+2); cg[i].p.Left:=p1.Left; cg[i].p.Height:=p1.Height;
        cg[i].p.OnResize:=CandPanelResize;
      cg[i].nameL:=TLabel.Create(Self); cg[i].nameL.Parent:=cg[i].p; cg[i].nameL.Name:='cg_'+inttostr(i)+'_nameL'; cg[i].nameL.Caption:='';
        cg[i].nameL.Font:=candL.Font; cg[i].nameL.Left:=candL.Left; cg[i].nameL.Top:=candL.Top; cg[i].nameL.OnClick:=alesLClick;
      cg[i].polL:=TLabel.Create(Self); cg[i].polL.Parent:=cg[i].p; cg[i].polL.Name:='cg_'+inttostr(i)+'_polL'; cg[i].polL.Caption:='';
        cg[i].polL.Font:=polCandL.Font; cg[i].polL.Top:=polCandL.Top; cg[i].polL.AutoSize:=polCandL.AutoSize; cg[i].polL.Alignment:=polCandL.Alignment; cg[i].polL.OnClick:=polALClick;
      cg[i].procL:=TLabel.Create(Self); cg[i].procL.Parent:=cg[i].p; cg[i].procL.Name:='cg_'+inttostr(i)+'_procL'; cg[i].procL.Caption:='';
        cg[i].procL.Font:=procCandL.Font; cg[i].procL.Top:=procCandL.Top; cg[i].procL.AutoSize:=procCandL.AutoSize; cg[i].procL.Alignment:=procCandL.Alignment;
      cg[i].nrVotL:=TLabel.Create(Self); cg[i].nrVotL.Parent:=cg[i].p; cg[i].nrVotL.Name:='cg_'+inttostr(i)+'_nrVotL'; cg[i].nrVotL.Caption:='';
        cg[i].nrVotL.Font:=nrVotL.Font; cg[i].nrVotL.Top:=nrVotL.Top; cg[i].nrVotL.AutoSize:=nrVotL.AutoSize; cg[i].nrVotL.Alignment:=nrVotL.Alignment;
      cg[i].candImg:=TImage.Create(Self); cg[i].candImg.Parent:=cg[i].p; cg[i].candImg.Name:='cg_'+inttostr(i)+'_candImg';
        cg[i].candImg.Width:=candImg.Width; cg[i].candImg.Height:=candImg.Height;
        with cg[i].candImg do begin Left:=0; Top:=0; center:=true; stretch:=true; proportional:=true end;
      cg[i].polCandColImg:=TImage.Create(Self); cg[i].polCandColImg.Parent:=cg[i].p; cg[i].polCandColImg.Name:='cg_'+inttostr(i)+'_polCandColImg';
        cg[i].polCandColImg.Height:=candPolColImg.Height; cg[i].polCandColImg.Left:=0; cg[i].polCandColImg.Top:=candPolColImg.Top;
    end;
  //
  Acb.Clear; for i:=0 to na-1 do Acb.Items.Add(inttostr(a[i])); Acb.ItemIndex:=na-1;
  refBClick(F1)
end;

procedure TF1.Adugarealegerinoi1Click(Sender: TObject);
begin
  FAddRA.Show
end;

procedure TF1.Adugareformaiunepolitic1Click(Sender: TObject);
var s: string; x: integer;
begin
  if InputQuery('Adăugare formațiune politică', 'Scrieți numele scurt al noii formațiuni', s) then
    begin
      x:=FPozPartidinFP(s); if x<>-1 then begin showmessage('Numele scurt trebuie să fie unic; mai există deja o formațiune cu acest nume (I='+inttostr(x)+', "'+fp[x].NumeComplet+'")!'); exit end;
      inc(nfp); setlength(fp, nfp); fp[nfp-1].NumeScurt:=s; fp[nfp-1].NumeComplet:='Necunsocut'; fp[nfp-1].NumeDinListeOficial:='NECUNOSCUT';  fp[nfp-1].Culoare:=clGray;
      if FEdFP.Showing then FEdFP.OnShow(Adugareformaiunepolitic1) else FEdFP.Show;
      FEdFP.lb.ItemIndex:=FEdFP.lb.Items.IndexOf(s); FEdFP.lbClick(Adugareformaiunepolitic1); FEdFP.le2.SetFocus; FEdFP.le2.SelectAll
    end
end;

procedure TF1.alesLClick(Sender: TObject);
begin candShow:=TLabel(Sender).Caption; if FRCand.showing then FRCand.OnShow(alesL) else FRCand.show; FRCand.SetFocus end;

procedure TF1.appbMouseEnter(Sender: TObject);
begin
  showmessage('replace with simple shape or image')
end;

procedure TF1.CandPanelResize(Sender: TObject);
var x, w: word;
begin
  for x:=0 to ncg-1 do if Sender=cg[x].p then break; w:=cg[x].p.Width;
  cg[x].polL.Left:=round(3/5*w); cg[x].polL.Width:=round(w/5);
  cg[x].procL.Left:=round(4/5*w); cg[x].procL.Width:=round(w/5);
  cg[x].nrVotL.Left:=cg[x].procL.Left; cg[x].nrVotL.Width:=cg[x].procL.Width;
  cg[x].polCandColImg.Width:=cg[x].polL.Left+cg[x].polL.Width;
end;

procedure TF1.FormResize(Sender: TObject);
var i, h, w: word;
begin
  h:=F1.Height; w:=F1.Width;
  img.Left:=0; img.Top:=h div 2 - img.Height div 2;
  Panel1.Left:=900; Panel1.Width:=w-Panel1.Left-10; Panel1.Height:=450;
  appb.Width:=panel1.Width-round(2.5*appb.Left);
  //
  for i:=0 to njc-1 do
    begin
      lg[i].nameL.Caption:=jc[i].Nume;
      lg[i].nameL.Left:=jc[i].X-lg[i].nameL.Width div 2; lg[i].nameL.Top:=jc[i].Y-round((lg[i].nameL.Height*1.5)/2);
      lg[i].img.Top:=lg[i].nameL.Top+lg[i].nameL.Height;
      lg[i].img.Left:=lg[i].nameL.Left; lg[i].img.Width:=lg[i].nameL.Width;
    end;
  for i:=0 to noc-1 do
    begin
      cc[i].Left:=img.Left+oc[i].X-cc[i].Width div 2;
      cc[i].Top:=img.Top-9+oc[i].Y //works correctly for 1366x768
    end;
  for i:=0 to ncg-1 do
    begin
      cg[i].p.Width:=Panel1.Width-2*p1.Left
    end
end;

procedure TF1.FormShow(Sender: TObject);
  {*}procedure CenterWindow(x: TForm);
  begin x.Left:=Screen.Width div 2-x.Width div 2; x.Top:=Screen.Height div 2-x.Height div 2 end;
var i: word; tx: TverySimpleXML;
begin
  try
    tx:=TverySimpleXML.Create; tx.LoadFromFile('data\restart.xml');
    if not (strtodatetime(tx.Root.Attribute['when'])>=incsecond(now, -30)) then exit;
    case strtocase(tx.Root.Attribute['edit'], ['judete', 'orase']) of
    1: begin Editarecoordonate1Click(F1); FEdCJ.lb.ItemIndex:=FEdCJ.lb.Items.IndexOf(tx.Root.Attribute['what']); FEdCJ.lbClick(F1); CenterWindow(FEdCJ) end;
    2: begin Editarecoordonate2Click(F1); FEdCO.lb.ItemIndex:=FEdCO.lb.Items.IndexOf(tx.Root.Attribute['what']); FEdCO.lbClick(F1); CenterWindow(FEdCO) end;
    end;
  finally
    tx.Free;
    for i:=0 to noc-1 do if oc[i].Nume='Brasov, Brasov' then begin MouseOverCity(cc[i]); break end
  end;
end;

procedure TF1.Gndaci1Click(Sender: TObject);
begin
  showmessage('BUGS:'+dnl+'- when adding new votes - when creating party nort in db, city gets created as many times (or more?) as there are new parties (fixed!)')
end;

procedure TF1.imgClick(Sender: TObject);
begin
  if not (FEdCJ.Showing or FEdCO.Showing) then exit;
  if FEdCJ.Showing then
    begin
      jc[FEdCJ.lb.ItemIndex].X:=strtoint(copy(Label2.Caption, 1, pos(',', Label2.Caption)-1));
      jc[FEdCJ.lb.ItemIndex].Y:=strtoint(copy(Label2.Caption, pos(' ', Label2.Caption)+1, 4));
      FEdCJ.lbClick(img)
    end
  else
    begin
      oc[FEdCO.lb.ItemIndex].X:=strtoint(copy(Label2.Caption, 1, pos(',', Label2.Caption)-1));
      oc[FEdCO.lb.ItemIndex].Y:=strtoint(copy(Label2.Caption, pos(' ', Label2.Caption)+1, 4));
      FEdCO.lbClick(img)
    end
end;

procedure TF1.imgMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin Label2.Caption:=inttostr(x)+', '+inttostr(y) end;

procedure TF1.optBClick(Sender: TObject);
var p: TPoint;
begin getcursorpos(p); optM.Popup(p.X, p.Y) end;

procedure TF1.polALClick(Sender: TObject);
var s: string;
begin s:=TLabel(Sender).Caption; if pos(' ', s)<>0 then s:=copy(s, 1, pos(' ', s)-1); fpShow:=s; if FRFP.Showing then FRFP.OnShow(polAL) else FRFP.Show; FRFP.SetFocus end;

function Winner(l: TListaCandidati; ResultType: string): string;
var i, p: integer; max: real; r: string;
begin
  max:=0; p:=0;
  for i:=0 to l.nc-1 do if l.Candidat[i].Procentaj>max then begin max:=l.Candidat[i].Procentaj; p:=i end;
  case strtocase(ResultType, ['color', 'name', 'partid', 'partidS', 'voturi', '%']) of
  1: //color
    begin
      if (max=0) or (FPozPartidInFP(l.Candidat[p].Politica)=-1) then r:=ColorToHTML(clGray) else r:=ColorToHTML(fp[FPozPartidInFP(l.Candidat[p].Politica)].Culoare);
    end;
  2: //nume
    begin
      if (max=0) then r:='necunoscut' else r:=l.Candidat[p].Nume
    end;
  3: //nume partid
    begin
      if (max=0) or (FPozPartidInFP(l.Candidat[p].Politica)=-1) then r:='necunoscut' else r:=fp[FPozPartidInFP(l.Candidat[p].Politica)].NumeComplet
    end;
  4: //nume scurt partid
    begin
      if (max=0) then r:='necunoscut' else r:=l.Candidat[p].Politica
    end;
  5: //nr voturi
    begin
      if (max=0) or (FPozPartidInFP(l.Candidat[p].Politica)=-1) then r:='necunoscut' else r:=grupat(l.Candidat[p].NrVoturi)
    end;
  6: //procentaj
    begin
      if (max=0) or (FPozPartidInFP(l.Candidat[p].Politica)=-1) then r:='necunoscut' else r:=floattostr(l.Candidat[p].Procentaj)
    end;
  end;
  result:=r//; log('Winner() result = "'+r+'"')
end;

procedure PColJudImg(col: TColor; var img: TImage);
begin
  try
  img.Picture:=nil; img.Canvas.Brush.Color:=col;
  img.Canvas.FillRect(Rect(0, 0, img.Width, img.Height))
  except on E:Exception do showmessage('PColImg (col="'+colortohtml(col)+'"; img.Name="'+img.Name+'") ERROR;'+dnl+e.ClassName+': '+e.Message) end;
end;

procedure TF1.lgVisibility(Sender: TObject; x: word; viz: boolean);
begin
  //showmessage('lgVisibility: x='+inttostr(x)+' ("'+lg[x].nameL.Name+'" and "'+lg[x].img.Name+'"), viz='+inttostr(integer(viz)));
  lg[x].nameL.Visible:=viz; lg[x].img.Visible:=viz
end;

procedure TF1.locLClick(Sender: TObject);
begin locShow:=TLabel(Sender).Caption; if FRL.Showing then FRL.OnShow(locL) else FRL.Show; FRL.SetFocus end;

procedure TF1.ccVisibility(Sender: TObject; x: word; viz: boolean);
begin
  //showmessage('ccVisibility: x='+inttostr(x)+' ("'+cc[x].Name+'"), viz='+inttostr(integer(viz)));
  cc[x].Visible:=viz
end;

procedure TF1.Deschidereraportdelapornire1Click(Sender: TObject);
begin GenerateTextReport(true) end;

procedure TF1.Despre1Click(Sender: TObject);
begin
  showmessage('by BogdyBBA'+dnl+'v1.0 alpha - June 11th+, 2012'+nl+'v1.0 beta - June 29th+, 2012'+nl+'v1.0 - July 3rd, 2012')
end;

procedure TF1.refBClick(Sender: TObject);
var xa, i, mp, mpcj: word;
begin
  xa:=strtoint(Acb.Text); mp:=0; mpcj:=0;
  // judete
  for i:=0 to njc-1 do
    if FVotare(xa, 'pCJ', jc[i].Nume)<>-1 then
      begin inc(mpcj); PColJudImg(HTMLToColor(Winner(vot[FVotare(xa, 'pCJ', jc[i].Nume)].Candidati, 'color')), lg[i].img); lgVisibility(refb, i, true) end
    else
      lgVisibility(refb, i, false);
  //orase
  for i:=0 to noc-1 do
    if FVotare(xa, 'P', oc[i].Nume)<>-1 then
      begin inc(mp); cc[i].Brush.Color:=HTMLToColor(Winner(vot[FVotare(xa, 'P', oc[i].Nume)].Candidati, 'color')); cc[i].Visible:=true end
      else cc[i].Visible:=false;
  log('Am afișast '+inttostr(mp)+' primari și '+inttostr(mpcj)+' președinți CJ pentru '+Acb.Text)
end;

procedure TF1.Restartprogram1Click(Sender: TObject);
begin ShellExecute(Handle, 'open', PChar('AL12.exe'), nil, nil, SW_SHOW); exitBClick(Restartprogram1) end;

procedure TF1.Setaresexcandidate1Click(Sender: TObject);
begin FSSC.Show end;

procedure TF1.Vizualizarerezultatepecandidai1Click(Sender: TObject);
begin candShow:='Christian Macedonschi'; FRCand.Show; FRCand.SetFocus end;

procedure TF1.Vizualizarerezultatepeformaiuni1Click(Sender: TObject);
begin fpShow:='FDGR'; FRFP.Show; FRFP.SetFocus end;

procedure TF1.Vizualizarerezultatepelocaii1Click(Sender: TObject);
begin locShow:='Brasov, Brasov'; FRL.Show; FRL.SetFocus end;

procedure TF1.MouseOverCity(Sender: TObject);
var x, xa, xv, i: word; xs: string;
begin
  for x:=0 to noc-1 do if Sender=cc[x] then break; lastCC:=cc[x].Name;
  xa:=strtoint(Acb.Text); if FVotare(xa, 'P', oc[x].Nume)=-1 then exit else xv:=FVotare(xa, 'P', oc[x].Nume);
  functieL.Caption:='Primar';
  locL.Caption:=oc[x].Nume; alesL.Caption:=Winner(vot[xv].Candidati, 'name');
  try alesImg.Picture.LoadFromFile('img\P_'+Winner(vot[xv].Candidati, 'name')+'.jpg') except alesImg.Picture.LoadFromFile('img\P'+vot[xv].Candidati.Candidat[0].Sex+'.jpg') end;
  polACImg.Width:=alesL.Width; PColJudImg(HTMLToColor(Winner(vot[xv].Candidati, 'color')), polACImg);
  polAL.Caption:=Winner(vot[xv].Candidati, 'partidS')+' ('+Winner(vot[xv].Candidati, 'partid')+')';
  appb.Max:=vot[xv].TotalVoturi; appb.Position:=vot[xv].Candidati.Candidat[0].NrVoturi;
  if vot[xv].Candidati.Candidat[0].Procentaj<50 then appb.State:=pbsError else if vot[xv].Candidati.Candidat[0].Procentaj>65 then appb.State:=pbsNormal else appb.State:=pbsPaused;
  //
  if vot[xv].Candidati.nc>0 then
    begin
      if vot[xv].Candidati.Candidat[0].Procentaj<>0 then
        begin
          if vot[xv].TotalVoturi<>0 then xs:=' '+grupat(vot[xv].TotalVoturi) else xs:='';
          alesProcL.Caption:='Ales cu '+grupat(vot[xv].Candidati.Candidat[0].NrVoturi)+' ('+Winner(vot[xv].Candidati, '%')+'%) din'+xs+' voturi'
        end
      else alesProcL.Caption:='Ales...'
    end
  else alesProcL.Caption:='';
  //
  if vot[xv].Candidati.nc=2 then Label3.Caption:='Contracandidatul...'
  else if vot[xv].Candidati.nc>6 then Label3.Caption:='Contracandidații 2-6 din '+inttostr(vot[xv].Candidati.nc)
  else if vot[xv].Candidati.nc>2 then Label3.Caption:='Contracandidații 2-'+inttostr(vot[xv].Candidati.nc)
  else Label3.caption:='';
  //
  for i:=0 to ncg-1 do
    begin
      cg[i].p.Visible:=i<vot[xv].Candidati.nc-1;
      if cg[i].p.Visible then
        begin
          try cg[i].candImg.Picture.LoadFromFile('img\P_'+vot[xv].Candidati.Candidat[i+1].Nume+'.jpg') except cg[i].candImg.Picture.LoadFromFile('img\P'+vot[xv].Candidati.Candidat[i+1].Sex+'.jpg') end;
          cg[i].nameL.Caption:=vot[xv].Candidati.Candidat[i+1].Nume;
          cg[i].polL.Caption:=vot[xv].Candidati.Candidat[i+1].Politica;
          cg[i].procL.Caption:=floattostr(vot[xv].Candidati.Candidat[i+1].Procentaj)+'%';
          if vot[xv].TotalVoturi<>0 then
            begin if vot[xv].TotalVoturi>1 then cg[i].nrVotL.Caption:=grupat(vot[xv].Candidati.Candidat[i+1].NrVoturi)+' voturi' else cg[i].nrVotL.Caption:='1 vot' end
          else cg[i].nrVotL.Caption:='';
          if FPozPartidInFP(vot[xv].Candidati.Candidat[i+1].Politica)=-1 then PColJudImg(clGray, cg[i].polCandColImg)
          else PColJudImg(fp[FPozPartidInFP(vot[xv].Candidati.Candidat[i+1].Politica)].Culoare, cg[i].polCandColImg)
        end
    end
end;

procedure TF1.Openlogfile1Click(Sender: TObject);
begin ShellExecute(Handle, 'open', PChar('data\log.txt'), nil, nil, SW_SHOW) end;

end.
