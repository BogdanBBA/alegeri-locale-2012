object FRFP: TFRFP
  Left = 0
  Top = 0
  BorderIcons = [biMinimize, biMaximize]
  Caption = 'Rezultate Forma'#539'iuni Politice'
  ClientHeight = 385
  ClientWidth = 814
  Color = clBlack
  Font.Charset = ANSI_CHARSET
  Font.Color = clWhite
  Font.Height = -16
  Font.Name = 'Ubuntu'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  ScreenSnap = True
  WindowState = wsMaximized
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 20
  object Label1: TLabel
    Left = 32
    Top = 54
    Width = 65
    Height = 20
    Caption = 'Nr. voturi'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -16
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 32
    Top = 78
    Width = 58
    Height = 20
    Caption = 'Procent'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -16
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
  end
  object fpcb: TComboBox
    Left = 8
    Top = 22
    Width = 506
    Height = 33
    Style = csDropDownList
    DropDownCount = 20
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -21
    Font.Name = 'Ubuntu Light'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnChange = fpcbChange
  end
  object Acb: TComboBox
    Left = 520
    Top = 22
    Width = 114
    Height = 35
    Style = csDropDownList
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -21
    Font.Name = 'Ubuntu Light'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = fpcbChange
  end
  object closeB: TButton
    Left = 640
    Top = 8
    Width = 150
    Height = 57
    Caption = #206'nchide'
    Font.Charset = ANSI_CHARSET
    Font.Color = 14337153
    Font.Height = -27
    Font.Name = 'Ubuntu'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    OnClick = closeBClick
  end
  object ch: TChart
    Left = 8
    Top = 152
    Width = 782
    Height = 225
    Title.Text.Strings = (
      'TChart')
    OnClickSeries = chClickSeries
    View3D = False
    TabOrder = 3
    object Series1: THorizBarSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = True
      Gradient.Direction = gdLeftRight
      XValues.Name = 'Bar'
      XValues.Order = loNone
      YValues.Name = 'Y'
      YValues.Order = loAscending
    end
  end
  object rb1: TRadioButton
    Left = 16
    Top = 56
    Width = 138
    Height = 17
    Checked = True
    TabOrder = 4
    TabStop = True
    OnClick = fpcbChange
  end
  object rb2: TRadioButton
    Left = 16
    Top = 80
    Width = 138
    Height = 17
    TabOrder = 5
    OnClick = fpcbChange
  end
  object Panel1: TPanel
    Left = 8
    Top = 103
    Width = 305
    Height = 48
    BevelOuter = bvNone
    TabOrder = 6
    object Label3: TLabel
      Left = 154
      Top = 0
      Width = 30
      Height = 20
      Caption = 'mari'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Ubuntu'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 154
      Top = 24
      Width = 27
      Height = 20
      Caption = 'mici'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Ubuntu'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 8
      Top = 12
      Width = 293
      Height = 20
      Caption = 'Afi'#537'eaz'#259' cele mai                                 valori'
    end
    object vrb1: TRadioButton
      Left = 137
      Top = 2
      Width = 53
      Height = 17
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = fpcbChange
    end
    object vrb2: TRadioButton
      Left = 137
      Top = 26
      Width = 53
      Height = 17
      TabOrder = 1
      OnClick = fpcbChange
    end
    object Edit1: TEdit
      Left = 190
      Top = 10
      Width = 59
      Height = 29
      Alignment = taCenter
      Font.Charset = ANSI_CHARSET
      Font.Color = 5384468
      Font.Height = -17
      Font.Name = 'Ubuntu'
      Font.Style = [fsBold]
      NumbersOnly = True
      ParentFont = False
      TabOrder = 2
      Text = '10'
      OnChange = fpcbChange
    end
  end
  object Panel2: TPanel
    Left = 392
    Top = 63
    Width = 398
    Height = 88
    BevelOuter = bvNone
    TabOrder = 7
    object alesL: TLabel
      Left = 18
      Top = 1
      Width = 61
      Height = 31
      Cursor = crHandPoint
      Caption = 'alesL'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -24
      Font.Name = 'Ubuntu'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = alesLClick
    end
    object Label6: TLabel
      Left = 18
      Top = 32
      Width = 52
      Height = 21
      Caption = 'Label6'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -17
      Font.Name = 'Ubuntu'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 18
      Top = 56
      Width = 48
      Height = 18
      Caption = 'Label7'
      Font.Charset = ANSI_CHARSET
      Font.Color = 16766429
      Font.Height = -16
      Font.Name = 'Ubuntu Mono'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel
      Left = 76
      Top = 32
      Width = 52
      Height = 21
      Cursor = crHandPoint
      Caption = 'Label8'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8314055
      Font.Height = -17
      Font.Name = 'Ubuntu'
      Font.Style = []
      ParentFont = False
      OnClick = Label8Click
    end
  end
end
