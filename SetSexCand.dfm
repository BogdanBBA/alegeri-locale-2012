object FSSC: TFSSC
  Left = 0
  Top = 0
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsSingle
  Caption = 'Setare sex candidate'
  ClientHeight = 379
  ClientWidth = 547
  Color = clBlack
  Font.Charset = ANSI_CHARSET
  Font.Color = clWhite
  Font.Height = -16
  Font.Name = 'Ubuntu'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  ScreenSnap = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 20
  object Label1: TLabel
    Left = 16
    Top = 8
    Width = 502
    Height = 20
    Caption = 
      'Scrie'#539'i lista de nume feminine pentru care candida'#539'ii sunt de fa' +
      'pt femei:'
  end
  object Label2: TLabel
    Left = 16
    Top = 34
    Width = 400
    Height = 17
    Caption = 
      'Aten'#539'ie! Nu introduce'#539'i '#537'i nume care pot fi '#537'i feminine '#537'i de fa' +
      'milie,'
    Font.Charset = ANSI_CHARSET
    Font.Color = clSilver
    Font.Height = -13
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 16
    Top = 303
    Width = 47
    Height = 20
    Caption = 'Label3'
  end
  object Label4: TLabel
    Left = 16
    Top = 50
    Width = 393
    Height = 17
    Caption = 
      'sau care pot face parte din nume masculine ("maria" din "marian"' +
      ').'
    Font.Charset = ANSI_CHARSET
    Font.Color = clSilver
    Font.Height = -13
    Font.Name = 'Ubuntu'
    Font.Style = []
    ParentFont = False
  end
  object mem: TMemo
    Left = 16
    Top = 81
    Width = 510
    Height = 216
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -19
    Font.Name = 'Ubuntu Mono'
    Font.Style = []
    Lines.Strings = (
      'loredana'
      'cristina'
      'olguta'
      'ioana'
      'ionela'
      'anca'
      'ancuta'
      'roxana'
      'vanessa'
      'corina'
      'teodora'
      'eliza'
      'luiza'
      'elise'
      'nicoleta'
      'raluca'
      'diana'
      'patrisia'
      'delia'
      'laura'
      'denisa'
      'miruna'
      'marinela'
      'dana'
      'stefana'
      'stefania'
      'daniela'
      'alexandra'
      'andreea'
      'monica'
      'flavia'
      'irina'
      'elena'
      'adelina'
      'alina'
      'doina'
      'ecaterina'
      'felicia'
      'florentina'
      'gabriela'
      'georgeta'
      'georgiana'
      'larisa'
      'lena'
      'livia'
      'lucia'
      'madalina'
      'marcela'
      'maria'
      'mariana'
      'mihaela'
      'melania'
      'mioara'
      'mirela'
      'nadia'
      'oana'
      'octavia'
      'olivia'
      'patricia'
      'rebeca'
      'patricia'
      'rodica'
      'ruxandra'
      'silvia'
      'simona'
      'sonia'
      'sorina'
      'stela'
      'valeria'
      'viorica'
      'zoe'
      'tania')
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 0
    OnChange = memChange
  end
  object closeB: TButton
    Left = 376
    Top = 303
    Width = 150
    Height = 57
    Caption = #206'nchide'
    Font.Charset = ANSI_CHARSET
    Font.Color = 14337153
    Font.Height = -27
    Font.Name = 'Ubuntu'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = closeBClick
  end
  object Button1: TButton
    Left = 220
    Top = 303
    Width = 150
    Height = 57
    Caption = 'Schimb'#259
    Font.Charset = ANSI_CHARSET
    Font.Color = 14337153
    Font.Height = -27
    Font.Name = 'Ubuntu'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    OnClick = Button1Click
  end
end
