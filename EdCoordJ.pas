﻿unit EdCoordJ;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TFEdCJ = class(TForm)
    lb: TListBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    closeB: TButton;
    saveB: TButton;
    saveT: TTimer;
    Label13: TLabel;
    procedure closeBClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lbClick(Sender: TObject);
    procedure saveBClick(Sender: TObject);
    procedure saveTTimer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FEdCJ: TFEdCJ;

implementation

{$R *.dfm}

uses DataTypes, functii, AL12_pas;

procedure TFEdCJ.closeBClick(Sender: TObject);
begin F1.refBClick(closeB); FEdCJ.Close end;

procedure TFEdCJ.FormShow(Sender: TObject);
var i: word;
begin
  if njc=0 then begin showmessage('Nu sunt judete de editat.'); closeBClick(FEdCJ); exit end;
  Label13.Caption:='Județe: '+inttostr(njc); lb.Clear; for i:=0 to njc-1 do lb.Items.Add(jc[i].Nume); lb.ItemIndex:=0; lbClick(FEdCJ)
end;

procedure TFEdCJ.lbClick(Sender: TObject);
var i: word;
begin
  F1.OnResize(lb);
  for i:=0 to njc-1 do F1.lgVisibility(lb, i, i<>lb.ItemIndex);
  Label3.Caption:='"'+jc[lb.ItemIndex].Nume+'"';
  label5.Caption:=inttostr(jc[lb.ItemIndex].X)+', '+inttostr(jc[lb.ItemIndex].Y)
end;

procedure TFEdCJ.saveBClick(Sender: TObject);
begin
  lbClick(saveB); SaveDatatoXml('coord');
  saveB.Caption:='OK'; saveB.Enabled:=false; saveT.Enabled:=true
end;

procedure TFEdCJ.saveTTimer(Sender: TObject);
begin
  saveT.Enabled:=false; saveB.Caption:='Salvare coord.'; saveB.Enabled:=true
end;

end.
