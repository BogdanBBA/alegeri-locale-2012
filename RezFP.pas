﻿unit RezFP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, TeEngine, ExtCtrls, TeeProcs, Chart, Series;

type
  TFRFP = class(TForm)
    fpcb: TComboBox;
    Acb: TComboBox;
    closeB: TButton;
    ch: TChart;
    Series1: THorizBarSeries;
    Label1: TLabel;
    Label2: TLabel;
    rb1: TRadioButton;
    rb2: TRadioButton;
    Panel1: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    vrb1: TRadioButton;
    vrb2: TRadioButton;
    Label5: TLabel;
    Edit1: TEdit;
    Panel2: TPanel;
    alesL: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    procedure FormShow(Sender: TObject);
    procedure fpcbChange(Sender: TObject);
    procedure closeBClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure chClickSeries(Sender: TCustomChart; Series: TChartSeries;
      ValueIndex: Integer; Button: TMouseButton; Shift: TShiftState; X,
      Y: Integer);
    procedure Label8Click(Sender: TObject);
    procedure alesLClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRFP: TFRFP;
  x1, x2: integer;

implementation

{$R *.dfm}

uses AL12_pas, functii, DataTypes, RezLoc, RezCand;

procedure TFRFP.closeBClick(Sender: TObject);
begin
  FRFP.Close
end;

procedure TFRFP.FormResize(Sender: TObject);
begin
  closeB.Left:=FRFP.Width-closeB.Width-31; Panel2.Width:=closeB.Left-Panel2.Left+closeB.Width;
  ch.Width:=closeB.Left-ch.Left+closeB.Width; ch.Height:=FRFP.Height-ch.Top-40
end;

procedure TFRFP.FormShow(Sender: TObject);
var i: word;
begin
  Acb.Items:=F1.Acb.Items; Acb.ItemIndex:=F1.Acb.ItemIndex; fpcb.Items.Clear; fpcb.Sorted:=true;
  for i:=0 to nfp-1 do fpcb.Items.Add(fp[i].NumeScurt+' ('+fp[i].NumeComplet+')');
  fpcb.ItemIndex:=fpcb.Items.IndexOf(fp[FPozPartidinFP(fpShow)].NumeScurt+' ('+fp[FPozPartidinFP(fpShow)].NumeComplet+')');
  fpcbChange(FRFP)
end;

procedure TFRFP.alesLClick(Sender: TObject);
begin candShow:=Tlabel(Sender).Caption; if FRCand.showing then FRCand.OnShow(alesL) else FRCand.show; FRCand.SetFocus end;

procedure TFRFP.chClickSeries(Sender: TCustomChart; Series: TChartSeries;
  ValueIndex: Integer; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var i, j: integer; sx: string;
begin
  FCandidat(copy(Series.Labels[ValueIndex], 1, pos(' [', Series.Labels[ValueIndex])-1), i, j);
  alesL.Caption:=vot[i].Candidati.Candidat[j].Nume;
  Label6.Caption:='Candidat '+vot[i].Candidati.Candidat[j].Politica+' pentru '+functie(vot[i].Functie)+' la ';
  Label8.Left:=Label6.Left+Label6.Width; Label8.Caption:=vot[i].Unde;
  if (vot[i].TotalVoturi<>0) and (vot[i].Candidati.Candidat[j].NrVoturi<>0) then sx:=' ('+floattostr(vot[i].Candidati.Candidat[j].Procentaj)+'%)' else sx:='?';
  Label7.Caption:='A obținut '+grupat(vot[i].Candidati.Candidat[j].NrVoturi)+sx+' din '+grupat(vot[i].TotalVoturi)+' voturi'
end;

procedure TFRFP.fpcbChange(Sender: TObject);
  {*}function plr(x: word): string; begin if x=1 then result:='candidat' else result:='candidați' end;
var x, p, i, j, tot, nafis: integer; safis, stot: longint; sx: string; l: TStringList; h: THorizBarSeries;
begin
  try strtoint(Edit1.Text) except exit end;
  x:=FPozPartidinFP(copy(fpcb.Text, 1, pos(' ', fpcb.Text)-1)); if x=-1 then begin showmessage('Nu am informații despre formațiunea politică pe care ai selectat-o !'); exit end;
  //
  ch.ClearChart; ch.View3D:=false;
  ch.Title.Font.Name:='Ubuntu'; ch.Title.Font.Style:=[fsBold]; ch.Title.Font.Size:=13; ch.Title.Font.Color:=$00522914;
  ch.SubTitle.Font:=ch.Title.Font; ch.SubTitle.Font.Size:=10; ch.SubTitle.Font.Color:=$007D532D;
  h:=THorizBarSeries.Create(Self); h.Active:=true;
  l:=TStringList.Create; l.Duplicates:=dupIgnore; l.Sorted:=false; tot:=0; nafis:=0; safis:=0; stot:=0;
  for i:=0 to nvot-1 do for j:=0 to vot[i].Candidati.nc-1 do //toti cantindatii
    if (vot[i].Candidati.Candidat[j].Politica=fp[x].NumeScurt) and (vot[i].Year=a[Acb.ItemIndex]) then //se potrivesc
      begin
        inc(tot); inc(stot, vot[i].Candidati.Candidat[j].NrVoturi);
        if rb1.Checked then l.Add(inttostr(vot[i].Candidati.Candidat[j].NrVoturi)+' '+inttostr(i)+'/'+inttostr(j))
        else l.Add(floattostr(vot[i].Candidati.Candidat[j].Procentaj)+' '+inttostr(i)+'/'+inttostr(j))
      end;
  if l.Count>1 then
    for i:=0 to l.Count-2 do for j:=i+1 to l.Count-1 do
      if strtofloat(copy(l[i], 1, pos(' ', l[i])-1))>strtofloat(copy(l[j], 1, pos(' ', l[j])-1)) then l.Exchange(i, j);
  //
  if l.Count>0 then
    begin if vrb1.Checked then begin x1:=max(0, l.Count-strtoint(Edit1.Text)); x2:=l.Count-1 end else begin x1:=0; x2:=min(l.Count-1, strtoint(Edit1.Text)-1) end end
    else begin x1:=-1; x2:=-1 end;
  //
  if x1+x2<>-2 then for p:=x2 downto x1 do
    begin
      i:=strtoint(copy(l[p], pos(' ', l[p])+1, pos('/', l[p])-pos(' ', l[p])-1)); j:=strtoint(copy(l[p], pos('/', l[p])+1, length(l[p])));
      inc(nafis); inc(safis, vot[i].Candidati.Candidat[j].NrVoturi); sx:=vot[i].Candidati.Candidat[j].Nume+' [';
      if pos(', ', vot[i].Unde)<>0 then sx:=sx+copy(vot[i].Unde, 1, pos(', ', vot[i].Unde)-1)+']' else sx:=sx+'j.'+vot[i].Unde+']';
      //
      if rb1.Checked then h.AddBar(strtoint(copy(l[p], 1, pos(' ', l[p])-1)), sx, fp[x].Culoare)
      else h.AddBar(strtofloat(copy(l[p], 1, pos(' ', l[p])-1)), sx, fp[x].Culoare);
    end;
  ch.AddSeries(h);
  ch.Title.Caption:='Rezultate '+fp[x].NumeScurt+' în '+Acb.Text;
  if l.Count>0 then sx:=', '+formatfloat('0.0', safis/stot*100)+'% partid' else sx:='';
  ch.SubTitle.Caption:='Afișez '+grupat(nafis)+' '+plr(nafis)+' ('+grupat(safis)+' voturi'+sx+') din '+grupat(tot)+' '+plr(tot)+' ('+grupat(stot)+' voturi)';
  Panel2.Visible:=l.Count>0; if Panel2.Visible then chClickSeries(ch, ch.Series[0], 0, mbLeft, [], 0, 0);
  l.Free
end;

procedure TFRFP.Label8Click(Sender: TObject);
begin locShow:=TLabel(Sender).Caption; if FRL.Showing then FRL.OnShow(Label8) else FRL.Show; FRL.SetFocus end;

end.
